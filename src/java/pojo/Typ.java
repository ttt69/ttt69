/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author efI
 */
public class Typ implements Serializable{

    private int id;
    private String name;

    public static final Typ Auftrag_V = new Typ(1, "Auftrag verrechenbar");
    public static final Typ Auftrag_NV = new Typ(2, "Auftrag nicht verrechenbar");
    public static final Typ Support_V = new Typ(3, "Support verrechenbar");
    public static final Typ Support_NV = new Typ(4, "Support nicht verrechenbar");
    public static final Typ Bug_V = new Typ(5, "Bug verrechenbar");
    public static final Typ Bug_NV = new Typ(6, "Bug nicht verrechenbar");

    public static final List<Typ> types = new ArrayList<Typ>() {
        {
            this.add(Auftrag_V);
            this.add(Auftrag_NV);
            this.add(Support_V);
            this.add(Support_NV);
            this.add(Bug_V);
            this.add(Bug_NV);
        }
    };

    public Typ(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static List<Typ> getTypes(){
        return types;
    }
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.id;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if(obj != null){
            if(obj instanceof Typ){
                Typ typ = (Typ) obj;
                
                if(typ.getId() == this.getId()){
                    return true;
                }
            }
        }
        
        return false;
    }
    
    

}

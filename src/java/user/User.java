/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package user;

import bean.Settings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pojo.Person;

/**
 *
 * @author efI
 */
public class User extends Person implements Serializable{

    private int id;
    private String username;
    private String password;
    private String rolle;
    private List <Integer> nummern = new ArrayList<>();
    private Settings settings;
    
    public User() {
    }
    
    public User(int id, String username){
        this.id = id;
        this.username = username;
    }

    public User(int id, String username, String password, String vorname, String nachname, String rolle) {
        super(id, vorname, nachname);
        this.id = id;
        this.username = username;
        this.password = password;
        this.rolle = rolle;
    }

    public User(int userid, String username, String password, int id, String vorname, String nachname) {
        super(id, vorname, nachname);
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(int id, String username, String password, String rolle) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.rolle = rolle;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRolle() {
        return rolle;
    }

    public void setRolle(String rolle) {
        this.rolle = rolle;
    }

    public void setNummern(List<Integer> nummern) {
        this.nummern = nummern;
    }

    public List<Integer> getNummern() {
        return nummern;
    }
    
    public void setNummer(int nummer){
        nummern.add(nummer);
    }
    
}

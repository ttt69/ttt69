/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import pojo.Typ;

/**
 *
 * @author efI
 */
public class Typ_Converter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        for (Typ t : Typ.getTypes()) {
            if (t.getName().equals(string)) {
                return t;
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        if (o != null) {
            Typ t = (Typ) o;

            return t.getName();
        }

        return null;
    }

}

//package daotopojo;
// 
//import database.DAO.OCPRDAO;
//import database.DAO.OCRDDAO;
// import database.DAO.OSCLDAO;
// import database.DAO.OUSRDAO;
//import java.util.ArrayList;
// import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.naming.NamingException;
//import pojo.Kunde;
//import pojo.Person;
//import pojo.Priority;
//import pojo.Status;
// import pojo.Ticket;
//import pojo.databasepojos.OCPR;
// import pojo.databasepojos.OSCL;
// import pojo.databasepojos.OUSR;
//import pojo.databasepojos.OCRD;
// 
// public class ConvertToPojo{
//     static Ticket t = null;
//     public static List<Ticket> tickets = new ArrayList<>();
//     public static List<Kunde> kunden = new ArrayList<>();
//     public static List<Person> ansprechpartner = new ArrayList<>();
//
//     public static void initFromDB(){
//         getAnsprechpartnerFromDB();
//         getKundenFromDB();
//         getTicketsFromDB();
//     }
//     
//     public static List<Ticket> getTicketsFromDB(){
//       List<OSCL> oscl = null;
//       List<OUSR> ousr = null;
//         try {
//             ousr = OUSRDAO.initOUSR();
//             oscl = OSCLDAO.initOSCL();
//         } catch (NamingException ex) {
//             Logger.getLogger(ConvertToPojo.class.getName()).log(Level.SEVERE, null, ex);
//         }
//       
//      
//       List<OCRD> ocrd = null;
//         try {
//             ocrd = OCRDDAO.initOCRD();
//         } catch (NamingException ex) {
//             Logger.getLogger(ConvertToPojo.class.getName()).log(Level.SEVERE, null, ex);
//         }
//         try {
//             oscl = OSCLDAO.initOSCL();
//         } catch (NamingException ex) {
//             Logger.getLogger(ConvertToPojo.class.getName()).log(Level.SEVERE, null, ex);
//         }
//         
//         for(OSCL o:oscl){
//            t.ticketnummer = Integer.parseInt(o.getCallID());
//            t.betreff = o.getSubject();
//            for(Kunde k: getKunden()){
//                if(k.getName().equals(o.getCustmrName())){
//                    t.kundenname = k.getName();
//                    t.kundennummer = Integer.parseInt(k.getNummer());
//                }
//            }
//            
//            
//             for(Person p:ansprechpartner){
//                if(p.getFirmenid().equals(t.kundennummer)){
//                    t.ansprechpartner = p;
//            }
//         
//            t.beschreibung = o.getDescrption();
//            t.endedat = o.getCloseDate();
//            t.fertigstellungstermin = o.getCloseDate().toString();
////            t.history;
//            t.kundenname = o.getCustmrName();
//            t.kundennummer = Integer.parseInt(o.getCustomer());
////            t.leistung;
////            t.letztbearb
////            t.lösungsmethode;
//
//            for(Priority prio: Priority.getPriorities())
//                if(prio.getName().equals(o.getPriority()))
//                    t.priorität = prio;
//            
//            t.produkt = o.getSubject();
//            t.startdat = o.getCreateDate();
//            
//            for(Status stat: Status.getStatusList()){
//                if(stat.getName().equals(o.getStatus()))
//                    t.status = stat;
//            }
////            t.supporttyp = o.get;
//
//            t.startdat = o.getCreateDate();
//            
//            for(Status s: Status.getStatusList())
//                if(s.getName().equals(o.getStatus()))
//                    t.status = s;
//            
//            
////            t.supporttyp =;
////            
////            t.vertrag;
////            t.zuständiger = o;
//           
//            int zustnr = Integer.parseInt(o.getAssignee());
//            for(OUSR ou : ousr)
//                if(Integer.parseInt(ou.getUSERID()) == zustnr)
//                    t.zuständiger = ou.getU_NAME();
//
//         }
//         }
//        return tickets;   
//     }
//         
//     
//     public static List<Kunde> getKundenFromDB(){
//         List<OCRD> ocrd = null;
//         List<Person> p = null;
//         Kunde k = null;
//         try {
//             ocrd = OCRDDAO.initOCRD();
//         } catch (NamingException ex) {
//             Logger.getLogger(ConvertToPojo.class.getName()).log(Level.SEVERE, null, ex);
//         }
//         
//         for(OCRD o:ocrd){
//             p = new ArrayList<>();
//             k = new Kunde();
//             k.setName(o.getCardName());
//             String kundennummer = o.getCardName();
//             kundennummer = kundennummer.substring(2, kundennummer.length());
//             k.setNummer(kundennummer);
//             for(Person pers: ansprechpartner){
//                 if(pers.getFirmenid().equals(k.getNummer())){
//                     p.add(pers);
//                 }
//             }
//             
//             k.setAnsprechpartner(p);
//             
//         }
//         
//         return kunden;
//     }
//     
//     public static List<Person> getAnsprechpartnerFromDB(){
//         List<OCPR> ocpr = null;
//         Person p;
//         ansprechpartner = new ArrayList<>();
//         try {
//             ocpr = OCPRDAO.initOCPR();
//         } catch (NamingException ex) {
//             Logger.getLogger(ConvertToPojo.class.getName()).log(Level.SEVERE, null, ex);
//         }
//         
//         for(OCPR o:ocpr){
//            String kundenname = o.getName();
//            String []name = kundenname.split(" ");
//            String kundennummer = o.getCntctCode();
//            kundennummer = kundennummer.substring(2, kundennummer.length());
//            p = new Person(Integer.parseInt(o.getCntctCode()), name[1], name[2]);
//            p.setFirmenid(o.getCardCode());
//             
//                    
//            
//             p.setNachname(o.getName());
//             p.setTelefon(o.getTel1());
//             p.setEmail(o.getE_MailL());
//             ansprechpartner.add(p);
//         }
//         
//         
//         return ansprechpartner;
//     }
//
//    public static List<Person> getAnsprechpartner() {
//        return ansprechpartner;
//    }
//
//    public static List<Kunde> getKunden() {
//        return kunden;
//    }
//
//    public static List<Ticket> getTickets() {
//        return tickets;
//    }
//     
//     
// }  
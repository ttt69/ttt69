/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database.DAO;

/**
 *
 * @author Florian
 */

//Klasse für Benutzer/Rollenverwaltung
public class TTT_TUSR {
    private String Code; //Primary Key
    private String Name;
    private String U_Password;
    private String U_Role;

    public TTT_TUSR() {
    }

    
    
    public TTT_TUSR(String Code, String Name, String U_Password, String U_Role) {
        this.Code = Code;
        this.Name = Name;
        this.U_Password = U_Password;
        this.U_Role = U_Role;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getU_Password() {
        return U_Password;
    }

    public void setU_Password(String U_Password) {
        this.U_Password = U_Password;
    }

    public String getU_Role() {
        return U_Role;
    }

    public void setU_Role(String U_Role) {
        this.U_Role = U_Role;
    }

    @Override
    public String toString() {
        return "TTT_TUSR{" + "Code=" + Code + ", Name=" + Name + ", U_Password=" + U_Password + ", U_Role=" + U_Role + '}';
    }

    
    
    
    
    
    
}

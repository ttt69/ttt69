/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OCTR implements Serializable{
    
    private String ContractID;
    private String CstmrCode;
    private String CstmrName;

    public OCTR() {
    }

    public OCTR(String ContractID, String CstmrCode, String CstmrName) {
        this.ContractID = ContractID;
        this.CstmrCode = CstmrCode;
        this.CstmrName = CstmrName;
    }

    public String getContractID() {
        return ContractID;
    }

    public void setContractID(String ContractID) {
        this.ContractID = ContractID;
    }

    public String getCstmrCode() {
        return CstmrCode;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj!=null&&obj instanceof OCTR){
            OCTR o=(OCTR) obj;
            if(this.ContractID.equals(o.ContractID) && this.CstmrCode.equals(o.CstmrCode)){
                ret=true;
            }
        }
        
        
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.ContractID);
        hash = 79 * hash + Objects.hashCode(this.CstmrCode);
        hash = 79 * hash + Objects.hashCode(this.CstmrName);
        return hash;
    }

    
    
    
    
    
    public void setCstmrCode(String CstmrCode) {
        this.CstmrCode = CstmrCode;
    }

    public String getCstmrName() {
        return CstmrName;
    }

    public void setCstmrName(String CstmrName) {
        this.CstmrName = CstmrName;
    }
    
    
    
}

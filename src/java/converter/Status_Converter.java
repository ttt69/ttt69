/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import pojo.State;

/**
 *
 * @author efI
 */
public class Status_Converter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        for (State s : State.getStatusList()) {
            if (s.getName().equals(string)) {
                return s;
            }
        }

        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        if (o != null) {
            State s = (State) o;

            return s.getName();
        }
        return null;
    }

}

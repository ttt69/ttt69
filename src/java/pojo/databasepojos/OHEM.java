/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OHEM implements Serializable{
    
    private String empID;
    private String lastName;
    private String firstName;
    private String sex;
    private String jobTitle;

    public OHEM() {
    }

    public OHEM(String empID, String lastName, String firstName, String sex, String jobTitle) {
        this.empID = empID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.sex = sex;
        this.jobTitle = jobTitle;
    }

    public String getEmpID() {
        return empID;
    }

    public void setEmpID(String empID) {
        this.empID = empID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj != null && obj instanceof OHEM){
            
            OHEM o=(OHEM) obj;
            
            if(this.empID.equals(o.empID)){
                ret=true;
            }
            
            
        }
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.empID);
        hash = 59 * hash + Objects.hashCode(this.lastName);
        hash = 59 * hash + Objects.hashCode(this.firstName);
        hash = 59 * hash + Objects.hashCode(this.sex);
        hash = 59 * hash + Objects.hashCode(this.jobTitle);
        return hash;
    }
    
    
    
    
}

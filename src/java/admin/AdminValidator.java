/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package admin;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Patrick
 */
public class AdminValidator implements Validator{

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        if(o == null){
            FacesMessage message = new FacesMessage();
            String messageStr = (String)uic.getAttributes().get("message");
            
            if (messageStr == null) {
                messageStr = "Bitte alle notwendigen Daten eingeben";
            }
            message.setDetail(messageStr);
            message.setSummary(messageStr);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
    
}

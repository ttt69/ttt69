/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import pojo.Customer;

/**
 *
 * @author efI
 */
public class Customer_Converter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        Customer c = null;
        
        if(string != null){
            System.out.println("!=null as Object");
            
            String[] splitted = string.split(" ");
            c = new Customer();
            c.setCnr(splitted[0]);
            c.setCname(splitted[1]);
        }
        
        return c;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    
        if(o != null){
            System.out.println("!=null as String");
            
            Customer c = (Customer) o;
            return c.getCnr() +  " " + c.getCname();
        }
        
        return null;
    }
    
}

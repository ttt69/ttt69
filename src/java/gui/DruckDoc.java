package gui;

import domain.Ticket_RMA;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

/**
 *
 * @author Snowpro
 */
public class DruckDoc implements Printable {

    Ticket_RMA t;
    int xpos;
    int ypos;

    DruckDoc(Ticket_RMA t1) {
        t = t1;
    }

    @Override
    public int print(Graphics g, PageFormat pf, int i) throws PrinterException {
        int[] colpositions = new int[3];
        int ret = Printable.NO_SUCH_PAGE;
        xpos = (int) pf.getImageableX();
        ypos = (int) pf.getImageableY();

        int fh = g.getFontMetrics().getHeight();

        int linesperpage = 5;// in der Regel zu berechnen: Druckbarer Bereich / Schrifthöhe+Zeilenabstand

        int linepos;

        int maxpages = 1;
        Font old = g.getFont();

        int spacing = fh * 2;// abstand zwischen den Zeilen
        if (i < maxpages) {
            ret = Printable.PAGE_EXISTS;
            linepos = header(g, pf, i);

           // g.drawString((i++)+"gay", xpos++, ypos);
            String str = t.getTicket_nr() + "";
            g.setFont(new Font("Arial", Font.ITALIC, 24)); //Schrift 24
            g.drawString("RMA-NR: " + str, xpos, ypos + spacing);
            g.drawString(t.getStatus(), xpos + 300, ypos + spacing);
            g.drawString(t.getPriori(), xpos + 450, ypos + spacing);

            g.drawString("Firma  ", xpos, ypos + spacing * 2); // Überschrift

            g.setFont(new Font("Arial", Font.BOLD, 13)); //Schrift 13
            g.drawString("Firmennr.:" + t.getFirma().getFirm_number(), xpos + 30, ypos + spacing * 3);
            g.drawString("Name:  " + t.getFirma().getName(), xpos + 30, ypos + spacing * 4);
            g.drawString("Kontakt:  " + t.getFirma().getKontakt(), xpos + 30, ypos + spacing * 5);
            g.drawString("Ort: " + t.getFirma().getFirm_adresse().getOrt(), xpos + 30, ypos + spacing * 6);
            g.drawString("Straße: " + t.getFirma().getFirm_adresse().getStraße(), xpos + 30, ypos + spacing * 7);
            g.drawString("Tel. Nr.: " + t.getFirma().getFirm_adresse().getTelnr(), xpos + 30, ypos + spacing * 8);

            g.setFont(new Font("Arial", Font.BOLD, 24)); //Schrift 24
            g.drawString("Artikel", xpos + 270, ypos + spacing * 2);
            g.setFont(new Font("Arial", Font.BOLD, 13)); //Schrift 13
            g.drawString("Name: " + t.getArtikel().getName(), xpos + 300, ypos + spacing * 3);
            g.drawString("Artikelnummer: " + t.getArtikel().getArtikel_nr(), xpos + 300, ypos + spacing * 4);
            g.drawString("Seriennummer: " + t.getSerial().getSerial_nr(), xpos + 300, ypos + spacing * 5);

            g.setFont(new Font("Arial", Font.BOLD, 24)); //Schrift 24
            g.drawString("Garantie", xpos, ypos + spacing * 10);
            g.setFont(new Font("Arial", Font.BOLD, 18)); //Schrift 18
            g.drawString("Garantieart: " + t.getSerial().getGarantie().getArt(), xpos + 30, ypos + spacing * 11);
            g.drawString("Garantiedauer: " + t.getSerial().getGarantie().getDauer(), xpos + 30, ypos + spacing * 12);

            g.setFont(new Font("Arial", Font.BOLD, 24)); //Schrift 24
            g.drawString("Lieferant", xpos, ypos + spacing * 14);
            g.setFont(new Font("Arial", Font.BOLD, 13)); //Schrift 13
            g.drawString("Name: " + t.getSerial().getLiefer().getName(), xpos + 30, ypos + spacing * 15);
            g.drawString("Ort: " + t.getSerial().getLiefer().getLief_adress().getOrt(), xpos + 30, ypos + spacing * 16);
            g.drawString("Straße: " + t.getSerial().getLiefer().getLief_adress().getStraße(), xpos + 30, ypos + spacing * 17);
            g.drawString("Tel. Nr. " + t.getSerial().getLiefer().getLief_adress().getTelnr(), xpos + 30, ypos + spacing * 18);

            g.drawString("Bemerkung:", xpos, ypos + spacing * 20);
            g.drawString(t.getBemerkung(), xpos + 30, ypos + spacing * 21);
            g.drawString("Beschreibung:", xpos, ypos + spacing * 23);
            g.drawString(t.getFehler_beschr(), xpos + 30, ypos + spacing * 24);

        }

//        if(i==0){
//            g.drawString("hello", xpos+10, ypos);
//        }
        return ret;
    }

    private int header(Graphics g, PageFormat pf, int i) {
        xpos = (int) pf.getImageableX();
        ypos = (int) pf.getImageableY();
        g.drawString("Advanced business Consulting", xpos, ypos + 9);
        return i;
    }

    private int fooder(Graphics g, PageFormat pf, int i) {
        //Seitenangabe in der Mitte
        int xpos = 0, ypos = 0;

        int middle = (int) (pf.getImageableWidth() / 2);
        String s = "Seite: " + (i + 1);
        //int breite = (int) (g.getFontMetrics().getStringBounds(s, g));

        g.drawString(s, xpos + 10, ypos + 9);

        return i;
    }

}

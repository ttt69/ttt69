/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.faces.context.FacesContext;
import user.User;
import user.Users;

/**
 *
 * @author efI
 */
public class Ticket implements Serializable {

    private int ticketnumber;    
    private String ticket_type;
    private State state;                    //Status
    private String subject;                  //Betreff
    private Person person_in_authority;      //Zuständiger
    private Priority priority;
    private Customer customer;
    private Date startdate;
    private Date endedate;
    private String contract;                 //Vertrag
    private String effort;                   //Leistung
    private String deadline;
    private String method_of_solution;
    private Typ supporttyp;
    private String product;
    private String address;
    private String description;
    private String channel_of_communication;
    private boolean allocation_of_project;   //Projektzuordnung
    private boolean internalprocess;         //internerVorgang
    private Person contactperson;
    private String client;                   //Mandant
    private Date lastmodified;
    private List<History> history_list = new ArrayList<>();
    private History hist = new History();
    private boolean msentbefore;    //Mailsentbefore
    private String firm_number;
    private String artikel_number;
    private String serial_number;

    public Ticket() {
    }

    public Ticket(int ticketnumber) {
        this.ticketnumber = ticketnumber;
    }

    public Ticket(int ticketnumber, String type, State state, String subject, Person person_in_authority, Priority priority, Customer customer, Date startdate, Date endedate, String contract, String effort, String deadline, String method_of_solution, Typ supporttyp, String product, String address, String description, String channel_of_communication, boolean allocation_of_project, boolean internalprocess, Person contactperson, String client, Date lastmodified, boolean msendbefore) {
        this.ticketnumber = ticketnumber;
        this.ticket_type = type;
        this.state = state;
        this.subject = subject;
        this.person_in_authority = person_in_authority;
        this.priority = priority;
        this.customer = customer;
        this.startdate = startdate;
        this.endedate = endedate;
        this.contract = contract;
        this.effort = effort;
        this.deadline = deadline;
        this.method_of_solution = method_of_solution;
        this.supporttyp = supporttyp;
        this.product = product;
        this.address = address;
        this.description = description;
        this.channel_of_communication = channel_of_communication;
        this.allocation_of_project = allocation_of_project;
        this.internalprocess = internalprocess;
        this.contactperson = contactperson;
        this.client = client;
        this.lastmodified = lastmodified;
        this.msentbefore = msendbefore;
        
        addTickettoUser();
    }

    public Ticket(int ticketnumber, String ticket_type, State state, String subject, Person person_in_authority, Priority priority, Date startdate, String description, Date lastmodified, String firm_number, String artikel_number, String serial_number) {
        this.ticketnumber = ticketnumber;
        this.ticket_type = ticket_type;
        this.state = state;
        this.subject = subject;
        this.person_in_authority = person_in_authority;
        this.priority = priority;
        this.startdate = startdate;
        this.description = description;
        this.lastmodified = lastmodified;
        this.firm_number = firm_number;
        this.artikel_number = artikel_number;
        this.serial_number = serial_number;
    }
    
    

    private void addTickettoUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users users = (Users) context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);
        List<User> user = users.getUsers();

        for (User u : user) {
            if (u.getUsername().equals(person_in_authority)) {
                int id = u.getId();
                u.setNummer(ticketnumber);
                user.set(id, u);
                users.setUsers(user);

                System.out.println(u.getUsername() + u.getId() + u.getNummern());
            }
        }
    }

    public int getTicketnumber() {
        return ticketnumber;
    }

    public void setTicketnumber(int ticketnumber) {
        this.ticketnumber = ticketnumber;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Person getPerson_in_authority() {
        return person_in_authority;
    }

    public void setPerson_in_authority(Person person_in_authority) {
        this.person_in_authority = person_in_authority;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }
    
    

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEndedate() {
        return endedate;
    }

    public void setEndedate(Date endedate) {
        this.endedate = endedate;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getEffort() {
        return effort;
    }

    public void setEffort(String effort) {
        this.effort = effort;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getMethod_of_solution() {
        return method_of_solution;
    }

    public void setMethod_of_solution(String method_of_solution) {
        this.method_of_solution = method_of_solution;
    }

    public Typ getSupporttyp() {
        return supporttyp;
    }

    public void setSupporttyp(Typ supporttyp) {
        this.supporttyp = supporttyp;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChannel_of_communication() {
        return channel_of_communication;
    }

    public void setChannel_of_communication(String channel_of_communication) {
        this.channel_of_communication = channel_of_communication;
    }

    public boolean isAllocation_of_project() {
        return allocation_of_project;
    }

    public void setAllocation_of_project(boolean allocation_of_project) {
        this.allocation_of_project = allocation_of_project;
    }

    public boolean isInternalprocess() {
        return internalprocess;
    }

    public void setInternalprocess(boolean internalprocess) {
        this.internalprocess = internalprocess;
    }

    public Person getContactperson() {
        return contactperson;
    }

    public void setContactperson(Person contactperson) {
        this.contactperson = contactperson;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Date getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(Date lastmodified) {
        this.lastmodified = lastmodified;
    }

    public List<History> getHistory_list() {
        return history_list;
    }

    public void setHistory_list(List<History> history_list) {
        this.history_list = history_list;
    }

    public History getHist() {
        return hist;
    }

    public void setHist(History hist) {
        this.hist = hist;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isMsentbefore() {
        return msentbefore;
    }

    public void setMsentbefore(boolean msentbefore) {
        this.msentbefore = msentbefore;
    }    

    public String getTicketType() {
        return ticket_type;
    }

    public void setTicketType(String s) {
        this.ticket_type = s;
    }

    public String getFirm_number() {
        return firm_number;
    }

    public void setFirm_number(String firm_number) {
        this.firm_number = firm_number;
    }

    public String getArtikel_number() {
        return artikel_number;
    }

    public void setArtikel_number(String artikel_number) {
        this.artikel_number = artikel_number;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }
    
    

    @Override
    public String toString() {
        return "Ticket{" + "ticketnumber=" + ticketnumber + ", state=" + state +
                ", subject=" + subject + ", person_in_authority=" + person_in_authority +
                ", priority=" + priority + ", customer=" + customer + ", startdate=" +
                startdate + ", endedate=" + endedate + ", contract=" + contract +
                ", effort=" + effort + ", deadline=" + deadline + ", method_of_solution="
                + method_of_solution + ", supporttyp=" + supporttyp + ", product=" +
                product + ", address=" + address + ", description=" + description +
                ", channel_of_communication=" + channel_of_communication +
                ", allocation_of_project=" + allocation_of_project + ", internalprocess=" +
                internalprocess + ", contactperson=" + contactperson + ", client="
                + client + ", lastmodified=" + lastmodified + ", history_list=" +
                history_list + ", hist=" + hist + ", msendbefore=" + msentbefore + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.ticketnumber;
        hash = 19 * hash + Objects.hashCode(this.state);
        hash = 19 * hash + Objects.hashCode(this.subject);
        hash = 19 * hash + Objects.hashCode(this.person_in_authority);
        hash = 19 * hash + Objects.hashCode(this.priority);
        hash = 19 * hash + Objects.hashCode(this.customer);
        hash = 19 * hash + Objects.hashCode(this.startdate);
        hash = 19 * hash + Objects.hashCode(this.endedate);
        hash = 19 * hash + Objects.hashCode(this.contract);
        hash = 19 * hash + Objects.hashCode(this.effort);
        hash = 19 * hash + Objects.hashCode(this.deadline);
        hash = 19 * hash + Objects.hashCode(this.method_of_solution);
        hash = 19 * hash + Objects.hashCode(this.supporttyp);
        hash = 19 * hash + Objects.hashCode(this.product);
        hash = 19 * hash + Objects.hashCode(this.address);
        hash = 19 * hash + Objects.hashCode(this.description);
        hash = 19 * hash + Objects.hashCode(this.channel_of_communication);
        hash = 19 * hash + (this.allocation_of_project ? 1 : 0);
        hash = 19 * hash + (this.internalprocess ? 1 : 0);
        hash = 19 * hash + Objects.hashCode(this.contactperson);
        hash = 19 * hash + Objects.hashCode(this.client);
        hash = 19 * hash + Objects.hashCode(this.lastmodified);
        hash = 19 * hash + Objects.hashCode(this.history_list);
        hash = 19 * hash + Objects.hashCode(this.hist);
        hash = 19 * hash + (this.msentbefore ? 1 : 0);
        return hash;
    }    

    @Override
    public boolean equals(Object obj) {

        if (obj != null) {
            if (obj instanceof Ticket) {
                Ticket ticket = (Ticket) obj;

                if (ticket.getTicketnumber() == this.getTicketnumber()) {
                    return true;
                }
            }
        }
        return false;
    }
}

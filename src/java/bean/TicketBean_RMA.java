package bean;

import bean.LoginBean;
import bean.TicketBean;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import domain.Adresse;
import domain.Artikel;
import domain.Firma;
import domain.Garantie;
import domain.Lieferant;
import domain.Serial;
import domain.Ticket_RMA;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import pojo.Person;
import pojo.Priority;
import pojo.State;
import pojo.Ticket;

/**
 *
 * @author Alexander Feiner i18n templates ajax primefaces
 */
public class TicketBean_RMA {

    //Instance for Settings-Bean
    FacesContext context1 = FacesContext.getCurrentInstance();
    TicketBean ticketBean = (TicketBean) context1.getApplication().evaluateExpressionGet(context1, "#{ticketBean}", TicketBean.class);
    LoginBean loginBean = (LoginBean) context1.getApplication().evaluateExpressionGet(context1, "#{loginBean}", LoginBean.class);



    private int ticket_number = ticketBean.getTickets().size(); //ticket Counter (Startnumber)
    private int change_ticket; //

    private Person auth;// saves the current Authority Person

    private Priority priority;

    private State state;

    private Ticket tick = new Ticket(0, null, null, null, null, null, null, null, null, null, null, null);
    private String selectedfirm = "000"; //returns firm_number of selected firm
    private String selectedart = "empty"; //returns article_number of selected article
    private String selectedserial = "empty"; //returns  serial_number of selected item 

    private Serial sel = new Serial(null, null, null); //saves the current data of of selected item
    private Artikel art = new Artikel(null, null, null); //saves the current data of of selected article
    private Firma firma = new Firma(" ", "", "", new Adresse("", "", ""), null); //saves the current data of of selected firm

    private String bem = ""; //saves the current !!!!!!!!!!!!
    private String beschr = ""; // saves the current !!!!!!!!!!!!

    Date start_d;
    Date change_d;

    private List<SelectItem> firmselects = new ArrayList<SelectItem>(); //saves the firmlist
    private List<SelectItem> artselects = new ArrayList<SelectItem>(); //saves the current article list
    private List<SelectItem> serialselects = new ArrayList<SelectItem>(); //saves the curren serial list
    private List<SelectItem> ticketselects = new ArrayList<SelectItem>();// saved tickets 

    private File file; // for download
    private StreamedContent doDownload; // for download

    String download_disable = "true";
    /*ONLY for Testdata-BEGIN*/
    private List<Serial> ser = new ArrayList<Serial>();
    private List<Serial> ser1 = new ArrayList<Serial>();
    private List<Serial> ser2 = new ArrayList<Serial>();
    private List<Serial> ser3 = new ArrayList<Serial>();
    private List<Serial> ser4 = new ArrayList<Serial>();
    private List<Serial> ser5 = new ArrayList<Serial>();
    private List<Serial> ser6 = new ArrayList<Serial>();

    private List<Artikel> artlist = new ArrayList<Artikel>();
    private List<Artikel> artlist1 = new ArrayList<Artikel>();
    private List<Artikel> artlist2 = new ArrayList<Artikel>();
    private List<Artikel> artlist3 = new ArrayList<Artikel>();
    private List<Artikel> artlist4 = new ArrayList<Artikel>();
    private List<Artikel> artlist5 = new ArrayList<Artikel>();
    private List<Artikel> artlist6 = new ArrayList<Artikel>();

    private List<Firma> firmlist = new ArrayList<Firma>();
    /*Testdata END*/

    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    SimpleDateFormat formatter2 = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss a");

    public TicketBean_RMA() {

        /*Testdata insert Begin*/
        ser.add(new Serial("555555", new Garantie("Bring-In", "2 Jahre"), new Lieferant("Siman", new Adresse("Mönichwald", "KV 432", "0660/54232"))));
        ser.add(new Serial("87654", new Garantie("Collect & Return", "1 Jahre"), new Lieferant("Kopper", new Adresse("Waldbach", "KV 432", "0660/542332"))));
        ser.add(new Serial("45622", new Garantie("Bring-In", "1,5 Jahre"), new Lieferant("Lorenzo", new Adresse("Neuburg", "KV 432", "0660/5432342"))));
        ser.add(new Serial("2357643", new Garantie("Collect & Return", "6 Jahre"), new Lieferant("Fandla", new Adresse("Hartberg", "KV 4332", "0660/54223432"))));
        ser.add(new Serial("876543", new Garantie("Bring-In", "8 Jahre"), new Lieferant("Kamper", new Adresse("Wien", "KV 43652", "0660/5423432"))));
        ser.add(new Serial("1234566", new Garantie("Collect & Return", "23 Jahre"), new Lieferant("Wingla", new Adresse("Graz", "KV 432332", "0660/54523432"))));

        ser1.add(new Serial("543", new Garantie("Collect & Return", "2 Jahre"), new Lieferant("Wingla", new Adresse("Graz", "KV 44532", "0660/54523432"))));
        ser1.add(new Serial("243432", new Garantie("Bring-In", "12 Jahre"), new Lieferant("Spreitz", new Adresse("Wien", "KV 786", "0660/54523432"))));
        ser1.add(new Serial("242342", new Garantie("Collect & Return", "11 Jahre"), new Lieferant("Putz", new Adresse("Hartber", "KV 678678", "0660/54523432"))));
        ser1.add(new Serial("574574", new Garantie("Bring-In", "2 Jahre"), new Lieferant("Fandla", new Adresse("Graz", "KV 432332", "0660/54523432"))));
        ser1.add(new Serial("135345", new Garantie("Collect & Return", "4 Jahre"), new Lieferant("Kamper", new Adresse("Graz", "KV 432332", "0660/54523432"))));

        ser2.add(new Serial("12353456", new Garantie("Collect & Return", "5 Jahre"), new Lieferant("Pichler", new Adresse("Graz", "KV 43532", "0660/54523432"))));
        ser2.add(new Serial("35344566", new Garantie("Bring-In", "6 Jahre"), new Lieferant("Putz", new Adresse("Graz", "KV 64362", "0660/54523432"))));
        ser2.add(new Serial("6352524566", new Garantie("Collect & Return", "17 Jahre"), new Lieferant("Wingla", new Adresse("Gra z", "KV 432332", "0660/54523432"))));

        ser3.add(new Serial("242342424566", new Garantie("Collect & Return", "14 Jahre"), new Lieferant("Wingla", new Adresse("Gleisdorf", "KV 432332", "0660/54523432"))));
        ser3.add(new Serial("35345466", new Garantie("Bring-In", "19 Jahre"), new Lieferant("Hofer", new Adresse("Weiz", "KV 432332", "0660/54523432"))));
        ser3.add(new Serial("12353453466", new Garantie("Collect & Return", "18 Jahre"), new Lieferant("Lidl", new Adresse("Rohrbauch", "KV 432332", "0660/54523432"))));

        ser4.add(new Serial("987654566", new Garantie("Bring-In", "17 Jahre"), new Lieferant("Kopper", new Adresse("Laftnit", "KV 432332", "0660/555432"))));
        ser4.add(new Serial("65434566", new Garantie("Bring-In", "14 Jahre"), new Lieferant("Efi", new Adresse("Harberg", "KV 5855332", "0660/54523432"))));
        ser4.add(new Serial("23456566", new Garantie("Collect & Return", "6 Jahre"), new Lieferant("Spreitz", new Adresse("Friedberg", "KV 8752", "0660/54523432"))));

        ser5.add(new Serial("2345676543", new Garantie("Voll", "7 Jahre"), new Lieferant("Wingla8", new Adresse("Gr az", "KV 432332", "0660/54523432"))));

        ser6.add(new Serial("18764566", new Garantie("Voll", "3 Jahre"), new Lieferant("Wingla9", new Adresse("Gr az", "KV 432332", "0660/54523432"))));
        ser6.add(new Serial("987654346", new Garantie("Voll", "7 Jahre"), new Lieferant("Wingla6", new Adresse("G raz", "KV 432332", "0660/54523432"))));
        ser6.add(new Serial("9876545", new Garantie("Voll", "8 Jahre"), new Lieferant("Wingla5", new Adresse("Gra z", "KV 432332", "0660/54523432"))));

        artlist1.add(new Artikel("Fernseher", "543df", ser));
        artlist1.add(new Artikel("Bildschirm", "5674ere", ser1));
        artlist1.add(new Artikel("Maus", "353454", ser5));

        artlist2.add(new Artikel("Laptop", "23622erd6", ser1));
        artlist2.add(new Artikel("Kabel", "73425322dj5", ser2));
        artlist2.add(new Artikel("Adapter", "35345dedj3", ser4));

        artlist3.add(new Artikel("Touchpad", "2334re235", ser3));
        artlist3.add(new Artikel("Webcam", "567de3j34", ser5));

        artlist4.add(new Artikel("Ladekabel", "234jd4235", ser));
        artlist4.add(new Artikel("Usb-Stick", "567dkd44", ser6));

        artlist5.add(new Artikel("Festplatte", "23423djt65", ser2));
        artlist5.add(new Artikel("Grafikkarte", "5674", ser5));

        artlist6.add(new Artikel("CD-Laufwerk", "2342dmdk535", ser6));
        artlist6.add(new Artikel("SD-Karte", "56cmd474", ser3));

        firmlist.add(new Firma("000", "Select Company", " ", new Adresse(" ", " ", " "), artlist));
        firmlist.add(new Firma("AT6543", "Apple", "Putz", new Adresse("St. Jakob", "Kirchenviertel 80", "0660/004343"), artlist1));
        firmlist.add(new Firma("US6543", "Microsoft", "Feiner", new Adresse("St. Jakob", "Lechenviertel 654", "0660/004343"), artlist2));
        firmlist.add(new Firma("WZ7654", "Advanced", "Michael", new Adresse("Wien", "Praterviertel 654", "0660/004343"), artlist3));
        firmlist.add(new Firma("MW6543", "Infonova", "Giggl", new Adresse("Hartberg", "Altstadtviertel 532", "0660/0333343"), artlist4));
        firmlist.add(new Firma("TU6543", "Unycom", "Halbwallner", new Adresse("Graz", "Löwengasse 4", "0660/6535222"), artlist5));
        firmlist.add(new Firma("RA6543", "Herbitschek", "Hannes", new Adresse("Ratten", "Hausstraße 45", "0660/4333422"), artlist6));
        firmlist.add(new Firma("RU6543", "SAP", "Zollner", new Adresse("Mürzzuschlag", "Gasse 20", "0660/4345642"), artlist5));
        firmlist.add(new Firma("DH4453", "BRZ", "Ziegerhofer", new Adresse("Wien", "Joseph-Fuchs Gasse 43", "0660/43985322"), artlist4));
        firmlist.add(new Firma("SH8765", "IT-Solutions", "Friesenbichler", new Adresse("Wien", "Hans-Strauss Straße 45", "0660/4847422"), artlist2));
        firmlist.add(new Firma("IC5646", "Sony", "Scherpichler", new Adresse("Graz", "Berliner Gasse 666", "0660/85456456"), artlist3));
        firmlist.add(new Firma("JS6544", "Samsung", "Leitner", new Adresse("Klagenfurt", "Ludwigweg 654", "0660/2562672"), artlist1));
        firmlist.add(new Firma("FF6464", "Acer", "Pötz", new Adresse("Innsbruck", "Dacherlweg 544", "0660/346342"), artlist6));

        /*Testdata insert END*/
        loadSelects();

        for (int i = 0; i < firmlist.size(); i++) {
            firmselects.add(new SelectItem(firmlist.get(i).getFirm_number(), firmlist.get(i).getName()));
        }

        kontaktupdate();
        newTicket();

    }

    /*------UPDATE-FUNKTIONS---------*/
    public void kontaktupdate() { //Updates Selected Firm Items(Kontakt...) and Articlelist
        Firma firm = new Firma(this.selectedfirm, null, null, null, null);

        int i = 0;
        int j = 0;

        //clear Section
        artselects.clear();
        serialselects.clear();
        art.setArtikel_nr("");
        sel.setGarantie(new Garantie("", ""));
        sel.setLiefer(new Lieferant("", new Adresse("", "", "")));
        this.selectedart = "empty";
        this.selectedserial = "empty";
        firma.setKontakt("");
        firma.setFirm_adresse(null);

        //Daten der ausgewählten Firma in die Variablen schreiben
        for (; i < firmlist.size(); i++) {
            if (firm.getFirm_number().equals(firmlist.get(i).getFirm_number())) {
                firma.setName(firmlist.get(i).getName());
                firma.setFirm_number(firmlist.get(i).getFirm_number());
                firma.setKontakt(firmlist.get(i).getKontakt());
                firma.setFirm_adresse(firmlist.get(i).getFirm_adresse());
                firma.setArtikellist(firmlist.get(i).getArtikellist());

                // Artiklesliste der ausgewählten Firma in die Selectlist schreiben
                for (; j < firmlist.get(i).getArtikellist().size(); j++) {
                    artselects.add(new SelectItem(firmlist.get(i).getArtikellist().get(j).getArtikel_nr(), //writes the selected article list in the ine menu
                            firmlist.get(i).getArtikellist().get(j).getName()));
                    download_disable = "true";
                }
            }
        }
    }

    public void artikelupdate() { //Updates Selected Article Items(Article_Number) and Serialnumberlist
        Artikel arti = new Artikel(null, this.selectedart, null);
        // Serial zurücksetzten
        this.selectedserial = "empty";
        sel.setGarantie(new Garantie("", ""));
        sel.setLiefer(new Lieferant("", new Adresse("", "", "")));
        art.setArtikel_nr(null);
        art.setSerial_list(null);
        serialselects.clear();

        //Daten des gewählten Artikel in die Variablen schreiben
        if (!this.selectedart.equals("empty")) {
            for (int i = 0; i < firma.getArtikellist().size(); i++) {
                if (arti.getArtikel_nr().equals(firma.getArtikellist().get(i).getArtikel_nr())) {
                    art.setArtikel_nr(firma.getArtikellist().get(i).getArtikel_nr());
                    art.setName(firma.getArtikellist().get(i).getName());

                    //Serialnumbers des Artikels in Seriallistschreiben
                    if (!firma.getArtikellist().get(i).getSerial_list().isEmpty()) {
                        art.setSerial_list(firma.getArtikellist().get(i).getSerial_list());
                        for (int j = 0; j < firma.getArtikellist().get(i).getSerial_list().size(); j++) {
                            serialselects.add(new SelectItem(firma.getArtikellist().get(i).getSerial_list().get(j).getSerial_nr(), // writes the needed serail list in the one menu
                                    firma.getArtikellist().get(i).getSerial_list().get(j).getSerial_nr()));
                            download_disable = "true";
                        }
                    }
                }
            }
        }
    }

    public void serialupdate() { // Updates all Serial Items(Garantie, Lieferant)
        System.out.println(this.selectedserial);
        Serial s = new Serial(this.selectedserial, null, null);
        sel.setGarantie(new Garantie("", ""));
        sel.setLiefer(new Lieferant("", new Adresse("", "", "")));

        //Daten des ausgewählten Artikel (zb. Garantie) in die Variablen schreiben
        if (!this.selectedserial.equals("empty")) {
            for (int i = 0; i < art.getSerial_list().size(); i++) {
                if (s.getSerial_nr().equals(art.getSerial_list().get(i).getSerial_nr())) {
                    sel.setSerial_nr(art.getSerial_list().get(i).getSerial_nr());
                    sel.setGarantie(art.getSerial_list().get(i).getGarantie());
                    sel.setLiefer(art.getSerial_list().get(i).getLiefer());
                    download_disable = "false";
                }
            }

        }
    }

    public void save() {

        boolean is_done = false;
        for (int i = 0; i < ticketBean.getTickets().size(); i++) { //neue Daten als Saved Ticket Speichern
            if (ticketBean.getTickets().get(i).getTicketnumber() == this.ticket_number) {
                if (checkTicket()) {
                    ticketBean.getTickets().get(i).setState(this.state);
                    ticketBean.getTickets().get(i).setPriority(this.priority);
                    ticketBean.getTickets().get(i).setPerson_in_authority(this.auth);
                    ticketBean.getTickets().get(i).setFirm_number(this.selectedfirm);
                    ticketBean.getTickets().get(i).setArtikel_number(this.selectedart);
                    ticketBean.getTickets().get(i).setSerial_number(this.selectedserial);
                    ticketBean.getTickets().get(i).setDescription(this.beschr);
                    ticketBean.getTickets().get(i).setSubject(this.bem);
                    ticketBean.getTickets().get(i).setLastmodified(this.change_d = new Date());
                    is_done = true;

                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage("Successful", "Ticket " + this.ticket_number + " überschrieben!"));
                    reset();
                }
            }
        }
        if (is_done == false) {
            if (checkTicket()) {
                Date start = new Date();
                ticketBean.getTickets().add(new Ticket(this.ticket_number, "Hardware", this.state, this.bem, this.auth, this.priority, start, this.beschr, start, this.selectedfirm, this.selectedart, this.selectedserial));

                ticketselects.add(new SelectItem(this.ticket_number));

                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Successful", "Ticket " + this.ticket_number + " gespeichert!"));
                newTicket();

            }
        }

    }

    public boolean checkTicket() {
        boolean check = false;
        if (this.selectedfirm.equals("000")) {
            Message("Warning", "Bitte zuerst Firma auswählen");
        } else {
            if (this.selectedart.equals("empty")) {
                Message("Warning", "Bitte zuerst Artikel auswählen");
            } else {
                if (this.selectedserial.equals("empty")) {
                    Message("Warning", "Bitte zuerst Serial auswählen");
                } else {
                    if (this.bem.equals("")) {
                        Message("Warning", "Bitte Betreff  eingeben");
                    } else {
                        check = true;
                    }
                }
            }
        }
        return check;
    }

    public void Message(String mes1, String mes2) {
        FacesMessage facemess = new FacesMessage(FacesMessage.SEVERITY_INFO, mes1, mes2);
        RequestContext.getCurrentInstance().showMessageInDialog(facemess);

    }

    public void newTicket() {

        this.ticket_number = ticketBean.getTickets().size() + 1;
        reset();

       // FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "mmmmm", "Echoes in eternity.");
        // RequestContext.getCurrentInstance().showMessageInDialog(message);
    }

    public String newRma() {
        newTicket();
        return "index_rma.xhtml";
    }

    public void loadSelects() {
        for (int i = 0; i < ticketBean.getTickets().size(); i++) {
            ticketselects.add(new SelectItem(ticketBean.getTickets().get(i).getTicketnumber()));
        }
    }

    public String open(Ticket d) { //öffnet das Ticket
        System.out.println("START OPEN");
        Ticket act_rma = d;
        String ret = null;

        if (act_rma.getTicketType().equals("Hardware")) {
            System.out.println(act_rma);
            this.priority = act_rma.getPriority();
            System.out.println(this.priority);
            this.state = act_rma.getState();
            System.out.println(state);
            this.auth = act_rma.getPerson_in_authority();
            System.out.println(auth);
            this.ticket_number = act_rma.getTicketnumber();
            System.out.println(ticket_number);
            this.selectedfirm = act_rma.getFirm_number();
            System.out.println(selectedfirm);
            kontaktupdate();
            this.selectedart = act_rma.getArtikel_number();
            artikelupdate();
            this.selectedserial = act_rma.getSerial_number();
            serialupdate();
            this.bem = act_rma.getSubject();
            System.out.println(bem);
            this.beschr = act_rma.getDescription();
            this.change_d = act_rma.getLastmodified();
            this.start_d = act_rma.getStartdate();
            download_disable = "false";
            System.out.println("CLOSE OPEN");
            ret = "index_rma.xhtml";
        }
        if (act_rma.getTicketType().equals("Software")) {
            ticketBean.setActTicket(act_rma);
            ret = "newTicket.xhtml";
        }
        return ret;
    }/* */




    public void reset() {  //Werte Zurücksetzten
        selectedfirm = "000";
        firma.setKontakt("");
        firma.setFirm_adresse(new Adresse("", "", ""));

        //this.auth = "Philip Pölz";
        this.bem = "";
        this.beschr = "";

       // this.start_date = "";
        //this.change_date = "";
        art.setArtikel_nr("");
        artselects.clear();

        serialselects.clear();
        sel.setGarantie(new Garantie("", ""));
        sel.setLiefer(new Lieferant("", new Adresse("", "", "")));
        this.download_disable = "true";
    }

    public String back() {
        String ret = "";
        if (loginBean.getRole().equals("Mitarbeiter")) {
            ret = "filter_myView_ma.xhtml";
        }
        if (loginBean.getRole().equals("Admin")) {
            ret = "filter_myView.xhtml";
        }
        return ret;
    }

    public void print() throws Exception {

        Ticket_RMA t = new Ticket_RMA(this.ticket_number, "", "", this.auth.toString(),
                this.firma, this.art, this.sel,
                this.beschr, this.bem);
        Document document = new Document();

        Date currentTime = new Date();
        if (file != null) {
            file.delete();
        }
        file = File.createTempFile("pdf", "pdf", null);

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

 //       ServletContext c = ((ServletContext)(FacesContext.getCurrentInstance().getExternalContext().getContext()));
        //       System.out.println(c.getRealPath(""));
//       Image image = Image.getInstance(c.getRealPath("")+"/WEB-INF/logos/logo.jpeg");
        /*BEGIN PRINT*/
        document.open();
        document.addTitle("RMA-Formular " + t.getTicket_nr());
        document.addAuthor("Autor");
        document.addCreationDate();
        document.addSubject("RMA");

        /*RMA NR*/
        document.add(new Paragraph("RMA - Formular             Nr: " + this.ticket_number + "        " + formatter2.format(currentTime), new Font(FontFamily.TIMES_ROMAN, 18, Font.BOLD, new BaseColor(23, 84, 112))));
        document.add(new Paragraph("Status:       " + t.getStatus() + "          Zuständiger:   " + t.getAuth()));
        document.add(new Paragraph("Priorität:     " + t.getPriori()));
        /*FIRMA*/
        document.add(new Paragraph("\nFirma ", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112)))); // Überschrift
        document.add(new Paragraph("Firmennr.:     " + t.getFirma().getFirm_number()));
        document.add(new Paragraph("Name:           " + t.getFirma().getName()));
        document.add(new Paragraph("Kontakt:        " + t.getFirma().getKontakt()));
        document.add(new Paragraph("Ort:                " + t.getFirma().getFirm_adresse().getOrt()));
        document.add(new Paragraph("Straße:          " + t.getFirma().getFirm_adresse().getStraße()));
        document.add(new Paragraph("Tel. Nr.:         " + t.getFirma().getFirm_adresse().getTelnr()));
        /*ARTIKEL*/
        document.add(new Paragraph("\nArtikel", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112))));
        document.add(new Paragraph("Name:                   " + t.getArtikel().getName()));
        document.add(new Paragraph("Artikelnummer:      " + t.getArtikel().getArtikel_nr()));
        document.add(new Paragraph("Seriennummer:      " + t.getSerial().getSerial_nr()));
        /*GARANTIE*/
        document.add(new Paragraph("\nGarantie", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112))));
        document.add(new Paragraph("Garantieart:           " + t.getSerial().getGarantie().getArt()));
        document.add(new Paragraph("Garantiedauer:      " + t.getSerial().getGarantie().getDauer()));
        /*LIEFERANT*/
        document.add(new Paragraph("\nLieferant", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112))));
        document.add(new Paragraph("Name:      " + t.getSerial().getLiefer().getName()));
        document.add(new Paragraph("Ort:          " + t.getSerial().getLiefer().getLief_adress().getOrt()));
        document.add(new Paragraph("Straße:    " + t.getSerial().getLiefer().getLief_adress().getStraße()));
        document.add(new Paragraph("Tel. Nr.:   " + t.getSerial().getLiefer().getLief_adress().getTelnr()));
        /*BEMERKUNG & BESCHREIBUNG*/
        document.add(new Paragraph("\nBemerkung:", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112))));
        document.add(new Paragraph(t.getBemerkung()));
        document.add(new Paragraph("\n\nBeschreibung:", new Font(FontFamily.HELVETICA, 11, Font.BOLDITALIC, new BaseColor(23, 84, 112))));
        document.add(new Paragraph(t.getFehler_beschr()));
        /*RECHTECK EINFÜGEN*/
        PdfContentByte cb = writer.getDirectContent();
        cb.saveState();
        /*Status*/ cb.rectangle(32, 736, 530, 38); //(Kooirdinate x, Koordinate y, Größe x, Größe y) 
       /*Firma*/ cb.rectangle(32, 597, 530, 108); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
       /*Artikel*/ cb.rectangle(32, 510, 530, 50); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
       /*Garantie*/ cb.rectangle(32, 435, 530, 38); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
       /*Lieferant*/ cb.rectangle(32, 335, 530, 70); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
       /*Bemerkung*/ cb.rectangle(32, 250, 530, 50); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
       /*Beschreibung*/ cb.rectangle(32, 133, 530, 100); //(Kooirdinate x, Koordinate y, Größe x, Größe y)
        cb.stroke();
        cb.restoreState();

        /*LOGO EINGÜGEN*/
//        image.setAbsolutePosition(438f, 50f);
//        document.add(image);
        document.close();
        /*END PRINT*/

        // noch vorher überpfrüfen ob Dokument existiert
        doDownload = new DefaultStreamedContent(new FileInputStream(file), "application/pdf", "RMA-" + t.getTicket_nr() + ".pdf");

    }

    /*________________________________________________________________________________
     |________________________________________________________________________________|
     |________________________________________________________________________________|
     |_____________________GETTER      &&       SETTER________________________________|
     |________________________________________________________________________________|
     |________________________________________________________________________________*/
    public StreamedContent getDoDownload() {
        return doDownload;
    }

    public String getSelectedfirm() {
        return selectedfirm;
    }

    public void setSelectedfirm(String selectedfirm) {
        this.selectedfirm = selectedfirm;
    }

    public Firma getFirma() {
        return firma;
    }

    public void setFirma(Firma firma) {
        this.firma = firma;
    }

    public List<Firma> getFirmlist() {
        return firmlist;
    }

    public void setFirmlist(List<Firma> firmlist) {
        this.firmlist = firmlist;
    }

    public List<SelectItem> getFirmselects() {
        return firmselects;
    }

    public void setFirmselects(List<SelectItem> firmselects) {
        this.firmselects = firmselects;
    }

    public List<Artikel> getArtlist1() {
        return artlist1;
    }

    public void setArtlist(List<Artikel> artlist) {
        this.artlist1 = artlist;
    }

    public List<SelectItem> getArtselects() {
        return artselects;
    }

    public void setArtselects(List<SelectItem> artselects) {
        this.artselects = artselects;
    }

    public String getSelectedart() {
        return selectedart;
    }

    public void setSelectedart(String selectedart) {
        this.selectedart = selectedart;
    }

    public Artikel getArt() {
        return art;
    }

    public void setArt(Artikel art) {
        this.art = art;
    }

    public List<SelectItem> getSerialselects() {
        return serialselects;
    }

    public void setSerialselects(List<SelectItem> serialselects) {
        this.serialselects = serialselects;
    }

    public Serial getSel() {
        return sel;
    }

    public void setSel(Serial sel) {
        this.sel = sel;
    }

    public String getSelectedserial() {
        return selectedserial;
    }

    public void setSelectedserial(String selectedserial) {
        this.selectedserial = selectedserial;
    }

    public int getTicket_number() {
        return ticket_number;
    }

    public void setTicket_number(int ticket_number) {
        this.ticket_number = ticket_number;
    }

    public String getBem() {
        return bem;
    }

    public void setBem(String bem) {
        this.bem = bem;
    }

    public String getBeschr() {
        return beschr;
    }

    public void setBeschr(String beschr) {
        this.beschr = beschr;
    }

    public int getChange_ticket() {
        return change_ticket;
    }

    public void setChange_ticket(int change_ticket) {
        this.change_ticket = change_ticket;
    }

    public List<SelectItem> getTicketselects() {
        return ticketselects;
    }

    public void setTicketselects(List<SelectItem> ticketselects) {
        this.ticketselects = ticketselects;
    }

    public String getFilePath() {
        return file.getAbsolutePath();
    }

    public Person getAuth() {
        return auth;
    }

    public void setAuth(Person auth) {
        this.auth = auth;
    }

    public String getDownload_disable() {
        return download_disable;
    }

    public void setDownload_disable(String download_disable) {
        this.download_disable = download_disable;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Date getChange_d() {
        return change_d;
    }

    public void setChange_d(Date change_d) {
        this.change_d = change_d;
    }

    public Date getStart_d() {
        return start_d;
    }

    public void setStart_d(Date start_d) {
        this.start_d = start_d;
    }



}

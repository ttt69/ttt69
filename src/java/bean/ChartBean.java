/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.model.chart.*;
import pojo.State;
import pojo.Ticket;
import pojo.TypforStatistic;
import user.User;
import user.Users;

/**
 *
 * @author efI
 */
public class ChartBean {

    private LineChartModel model_Status;
    private PieChartModel model_perUser;
    private LineChartModel model_All;
    private User mitarb;
    private User selectetUser;

    private List<User> mitarbeiter = new ArrayList<>();
    private List<SelectItem> mit = new ArrayList<>();

    FacesContext context = FacesContext.getCurrentInstance();
    TicketBean ticketBean = (TicketBean) context.getApplication().evaluateExpressionGet(context, "#{ticketBean}", TicketBean.class);

    public ChartBean() {

        statusChart();
        statisticPerUser();

        //-------- ALL-CHART ------------
//        model_All = new LineChartModel();
//
//        model_All.addSeries(inOperation);
//        model_All.setTitle("Statistic of the Tickets");
//        model_All.setLegendPosition("e");
//        model_All.setShowPointLabels(true);
//        model_All.getAxes().put(AxisType.X, new CategoryAxis("Months"));
//        Axis yAxis = model_All.getAxis(AxisType.Y);
//        yAxis.setLabel("Tickets");
//        yAxis.setMin(0);
//        yAxis.setMax(100);
    }

    public void statusChart() {

        //-------- STATUS-CHART ------------
        model_Status = new LineChartModel();

        ChartSeries openT_Series = new ChartSeries();
        ChartSeries inOperationT_Series = new ChartSeries();
        ChartSeries closedT_Series = new ChartSeries();

        //holt das aktuelle Jahr und den aktuellen Monat
        Calendar currentCal = new GregorianCalendar(TimeZone.getDefault());
        currentCal.setTime(new Date());
        int currentMonth = currentCal.get(Calendar.MONTH); //fängt bei 0 an
        int currentYear = currentCal.get(Calendar.YEAR);

        Calendar ticketCal = new GregorianCalendar(TimeZone.getDefault());
        int ticketMonth = -1;
        int ticketYear = -1;

        int i = 0;
        boolean contains = false;

        List<TypforStatistic> st_list = new ArrayList<>();

        for (Ticket t : ticketBean.getTickets()) {

            //holt das Start-Datum des Tickets
            ticketCal.setTime(t.getStartdate());
            ticketMonth = ticketCal.get(Calendar.MONTH);
            ticketYear = ticketCal.get(Calendar.YEAR);

            TypforStatistic st_typ = new TypforStatistic();
            st_typ.setYear(ticketYear);
            st_typ.setMonth(ticketMonth);

            for (TypforStatistic tfs : st_list) {

                //Überprüfe, ob ein bestimmter Eintrag in der Liste schon vorhanden ist
                if (tfs.getYear() == st_typ.getYear() && tfs.getMonth() == st_typ.getMonth()) {
                    st_typ = tfs;
                    contains = true;
                } else {
                    contains = false;
                }
            }
            
            System.out.println("t:" + ticketMonth);
            System.out.println("cur:" + currentMonth);
            while(currentMonth != 0){
                currentMonth -= 1;
                ticketMonth -= 1;
            }
            System.out.println("cur:" + currentMonth);
            
            if(currentMonth == 0){
                currentMonth = currentMonth + 12;
                currentMonth = currentMonth - ticketMonth;
            }
            System.out.println("t:" + ticketMonth);
            System.out.println("cur:" + currentMonth);
            
            /*
            System.out.println("dif:" + diffMonth);
            if(diffMonth < 0){
                diffMonth = diffMonth*-1;
            }
            System.out.println("dif:" + diffMonth);
                    */
            if (true) {
                //Erhöhe die jeweiligen Werte
                if (t.getState() == State.OFFEN) {
                    st_typ.setOpen(st_typ.getOpen() + 1);
                }
                if (t.getState() == State.IN_ARBEIT) {
                    st_typ.setInOperation(st_typ.getInOperation() + 1);
                }
                if (t.getState() == State.GESCHLOSSEN) {
                    st_typ.setClosed(st_typ.getClosed() + 1);
                }

                if (contains == true) {
                    st_list.remove(st_typ);
                    st_list.add(st_typ);
                } else {
                    st_list.add(st_typ);
                }
            }

        }

        System.out.println(st_list.toString());

        openT_Series.setLabel("Offen");
        inOperationT_Series.setLabel("In Arbeit");
        closedT_Series.setLabel("Geschlossen");

        //Zeichnet die Grafik
        for (TypforStatistic tfs : st_list) {
            openT_Series.set(tfs.getMonth(), tfs.getOpen());
        }
        model_Status.addSeries(openT_Series);

        for (TypforStatistic tfs : st_list) {
            inOperationT_Series.set(tfs.getMonth(), tfs.getInOperation());
        }
        model_Status.addSeries(inOperationT_Series);

        for (TypforStatistic tfs : st_list) {
            closedT_Series.set(tfs.getMonth(), tfs.getClosed());
        }
        model_Status.addSeries(closedT_Series);

        model_Status.setTitle("Status der Tickets");
        model_Status.setLegendPosition("e");
        model_Status.setShowPointLabels(true);
        Axis yAxis = model_Status.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(ticketBean.getTickets().size());
        yAxis.setLabel("Tickets");
        yAxis.setTickInterval("2");
        model_Status.getAxes().put(AxisType.X, new CategoryAxis("Months"));
    }

    public void statisticPerUser() {

        //-------- PERUSER-CHART ------------
        int open = 0;
        int inOperation = 0;
        int closed = 0;

        Users users = (Users) context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);

        mitarbeiter.add(new User(0, "Alle"));
        for (User u : users.getUsers()) {
            if (u.getRolle().equals("Mitarbeiter")) {
                mitarbeiter.add(u);
            }
        }
        
        for(User u: mitarbeiter){
            mit.add(new SelectItem(u.getUsername()));
        }
        
        
        for (Ticket t : ticketBean.getTickets()) {
            for (User u : mitarbeiter) {
                if (t.getPerson_in_authority().equals(u)) {
                    boolean check = false;
                    for(int us : u.getNummern())
                        if(t.getTicketnumber() == us)
                            check = true;
                    
                    if(check = false)
                    u.setNummer(t.getTicketnumber());
                }
            }
        }

        model_perUser = new PieChartModel();

        model_perUser.setTitle("Tickets je Miterabeiter");
        model_perUser.setLegendPosition("e");
        model_perUser.setFill(true);
        model_perUser.setShowDataLabels(true);
        model_perUser.setDiameter(50);

        open = 0;
        inOperation = 0;
        closed = 0;

        for (Ticket t : ticketBean.getTickets()) {

            if (t.getPerson_in_authority().equals("Florian Peischl")) { //equals(user.getUsername())

                if (t.getState() == State.OFFEN) {
                    open++;
                }

                if (t.getState() == State.IN_ARBEIT) {
                    inOperation++;
                }

                if (t.getState() == State.GESCHLOSSEN) {
                    closed++;
                }

            }
        }

    }

    public LineChartModel getModel_Status() {
        return model_Status;
    }

    public PieChartModel getModel_perUser() {
        return model_perUser;
    }

    public LineChartModel getModel_All() {
        return model_All;
    }

    public List<User> getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(List<User> mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public void showStatUser(User u) {
        int open = 0, inOperation = 0, closed = 0;
        if (u != null) {

            List<Ticket> tickets = ticketBean.getTickets();

//        
//        for(Integer i:u.getNummern()){
//            System.out.println(ticketBean.getTickets().get(i-1).getSubject());
//            int id = ticketBean.getTickets().get(i-1).getState().getId(); 
//            if( id == 1){
//                open++;
//            }else if(id == 2){
//                inOperation++;
//            }
//            else if (id== 3){
//                closed++; 
//            }
//            }
            if(u.getUsername().equals("Alle")){
                 for(Ticket t:tickets)
                     switch (t.getState().getId()) {
                        case 1:
                            open++;
                            break;
                        case 2:
                            inOperation++;
                            break;
                        case 3:
                            closed++;
                            break;
                    }
            }
            
            for (Ticket t : tickets) {
                if (t.getPerson_in_authority().getId() == u.getId()) {
                    switch (t.getState().getId()) {
                        case 1:
                            open++;
                            break;
                        case 2:
                            inOperation++;
                            break;
                        case 3:
                            closed++;
                            break;
                    }
                }
            }

//            if(open ==0 && inOperation == 0){
//            open = 1;
//            inOperation= 2;
//            }
            System.out.println("Offen: " + open + " in Arbeit: " + inOperation + " closed:  " + closed + " user" + u.getUsername());
            model_perUser.set("Offen " + open, open);
            model_perUser.set("In Arbeit " + inOperation, inOperation);
            model_perUser.setTitle(u.getUsername());
        }
    }
    


    public User getMitarb() {
        return mitarb;
    }

    public void setMitarb(User mitarb) {
        this.mitarb = mitarb;
    }

    public User getSelectetUser() {
        return selectetUser;
    }

    public void setSelectetUser(User selectetUser) {
        this.selectetUser = selectetUser;
    }

    public List<SelectItem> getMit() {
        return mit;
    }

    public void setMit(List<SelectItem> mit) {
        this.mit = mit;
    }
    
    

}

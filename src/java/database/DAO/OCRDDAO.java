/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.sap.smb.sbo.api.IAccountCategoryService;
import com.sap.smb.sbo.api.IAccountsService;
import com.sap.smb.sbo.api.IBusinessPartners;
import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.IUsers;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Florian
 */
public class OCRDDAO {
    //Business Partners
    //Daten die in CRD1 gespeichert sind, werden automatisch bei Lesen über OCRD mitausgelesen
    //daher ist kein CRD1DAO erforderlich

    public OCRDDAO() {
    }

    public List<IBusinessPartners> findAll() {

        List<IBusinessPartners> retVal = new LinkedList<>(); //ACHTUNG INDEX VON LINKED LIST BEGINNT BEI 0
        ICompany con = ConnectionManager.getInst().getConn();
        try {
            
            SBOCOMUtil.getUsers(con, Integer.SIZE);
           
            
            IRecordset rs = SBOCOMUtil.runRecordsetQuery(con, "Select CardCode from dbo.OCRD");
            List<String> bpcardcodes = new LinkedList<>();
            IBusinessPartners bp = SBOCOMUtil.newBusinessPartners(con);

            int anz_rows = 0;
            int count = rs.getFields().getCount().intValue(); //Anzahl der Spalten der Tabelle OCRD
            String FldVal;
            String FldName;

            while (rs.isEoF().equals(false)) {
                FldName = rs.getFields().item(0).getName();
                FldVal = String.valueOf(rs.getFields().item(0).getValue());
                bpcardcodes.add(FldVal);
                //Näcster Datensatz
                rs.moveNext();
            }

            System.out.println("GRÖSSE BP" + bpcardcodes.size());
            for (int i = 0; i < bpcardcodes.size(); i++) {
                bp = SBOCOMUtil.getBusinessPartners(con, bpcardcodes.get(i));
                retVal.add(bp);
                System.out.println(bp.getCardCode());
                System.out.println(bp.getCardName());
                System.out.println(i);
            }

        } catch (SBOCOMException ex) {

            System.out.println("Fehler in KLASSE: OCRDDAO");
            System.out.println("in FUNKTION: findAll");
            System.out.println(ex.getMessage());
        }

        return retVal;
    }

    public void updateOCRD(IBusinessPartners ib) {
        ib.update();
    }

    public void removeOCRD(IBusinessPartners ib) {
        ib.remove();
    }

    public void insertOCRD(IBusinessPartners ib) {
        ib.add();
    }
    
    public void test(){
        
        ICompany con = ConnectionManager.getInst().getConn();
        try {
            IUsers u;
            u=SBOCOMUtil.newUsers(con);
            
            u=SBOCOMUtil.getUsers(con,36);
            
            System.out.println(u.getUserName());
            System.out.println(u.getUserPassword());
            
            IAccountCategoryService i;
            
            
            
           // SBOCOMUtil.newAccountCategoryService(con.getCompanyService());
            
            IAccountsService ia= SBOCOMUtil.newAccountsService(con.getCompanyService());
            
            
//            u.setUserPassword("peischl");
//            u.setUserName("florian peischl");
//            u.setUserCode("florian");
//            u.setEMail("florian.peischl@gmail.com");
//            
//            u.add();
//            
            
            
        } catch (SBOCOMException ex) {
            Logger.getLogger(OCRDDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    

}

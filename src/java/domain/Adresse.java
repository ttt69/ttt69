package domain;

/**
 *
<<<<<<< HEAD
 * @author Snowpro
 */
public class Adresse {

    String ort;
    String straße;
    String telnr;

    public Adresse(String ort, String straße, String telnr) {
        this.ort = ort;
        this.straße = straße;
        this.telnr = telnr;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getStraße() {
        return straße;
    }

    public void setStraße(String straße) {
        this.straße = straße;
    }

    public String getTelnr() {
        return telnr;
    }

    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import pojo.Person;

/**
 *
 * @author efI
 */
public class Person_Converter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

        Person p = null;
        
        if(string != null){
            
            String[] splitted = string.split(" ");
            
            p = new Person();
            p.setFirstname(splitted[0]);
            p.setSurname(splitted[1]);
        }
        
        return p;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    
        if(o != null){
            Person p = (Person) o;
            
            return p.getFirstname()+ " " + p.getSurname();
        }
        
        return null;
    }
    
}

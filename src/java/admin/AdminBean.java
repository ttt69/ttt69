/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package admin;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author Patrick
 */
public class AdminBean {

    
    String newUsername;
    String newPasswort;
    String newRolle;
    String newVorname;
    String newNachname;
    String newId;
    List <SelectItem> rollen = new ArrayList<>();
    
    public AdminBean() {
        rollen.add(new SelectItem("Mitarbeiter"));
        rollen.add(new SelectItem("Kunde"));
        rollen.add(new SelectItem("Admin"));
        
    }
   
    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getNewPasswort() {
        return newPasswort;
    }

    public void setNewPasswort(String newPasswort) {
        this.newPasswort = newPasswort;
    }

    public String getNewRolle() {
        return newRolle;
    }

    public void setNewRolle(String newRolle) {
        this.newRolle = newRolle;
    }

    public String getNewNachname() {
        return newNachname;
    }

    public String getNewVorname() {
        return newVorname;
    }

    public void setNewNachname(String newNachname) {
        this.newNachname = newNachname;
    }

    public void setNewVorname(String newVorname) {
        this.newVorname = newVorname;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    public List<SelectItem> getRollen() {
        return rollen;
    }

    public void setRollen(List<SelectItem> rollen) {
        this.rollen = rollen;
    }
     
    

}

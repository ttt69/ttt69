/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Patrick
 */
public class Time extends Thread{

    List <Ticket> tickets = new ArrayList<>();
    
    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            for(Ticket t: tickets){
                GregorianCalendar now = new GregorianCalendar();  
                SimpleDateFormat df = new SimpleDateFormat( "dd.MM.yyyy HH:mm" );
                String date = df.format(now.getTime());
                if(date.equals(t.getDeadline())){
                     FacesContext context = FacesContext.getCurrentInstance();  
          
                    context.addMessage(null, new FacesMessage("Ticket fällig:" + t.getDescription()));  
                }
               
            }
            
            
            
        }
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Time() {
    }

    public Time(List<Ticket> tickets){
        this.tickets = tickets;
    }
    
}

package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Snowpro
 */
public class Firma {

    private String firm_number;
    private String name;
    private String kontakt;
    private Adresse firm_adresse;
    private List<Artikel> artikellist = new ArrayList<Artikel>();

    public Firma(String firm_number, String name, String kontakt, Adresse firm_adresse, List<Artikel> artlist) {
        this.firm_number = firm_number;
        this.name = name;
        this.kontakt = kontakt;
        this.firm_adresse = firm_adresse;
        this.artikellist = artlist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKontakt() {
        return kontakt;
    }

    public void setKontakt(String kontakt) {
        this.kontakt = kontakt;
    }

    public Adresse getFirm_adresse() {
        return firm_adresse;
    }

    public void setFirm_adresse(Adresse firm_adresse) {
        this.firm_adresse = firm_adresse;
    }

    public List<Artikel> getArtikellist() {
        return artikellist;
    }

    public void setArtikellist(List<Artikel> artikellist) {
        this.artikellist = artikellist;
    }

    public String getFirm_number() {
        return firm_number;
    }

    public void setFirm_number(String firm_number) {
        this.firm_number = firm_number;
    }

    @Override
    public String toString() {
        return name;
    }

}

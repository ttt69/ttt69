/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.IItems;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Florian
 */
public class OITMDAO {

    public OITMDAO() {
    }

    public List<IItems> findAll() {
        List<IItems> retVal = new LinkedList<>(); //ACHTUNG INDEX VON LINKED LIST BEGINNT BEI 0
        ICompany con = ConnectionManager.getInst().getConn();
        try {
            IRecordset rs = SBOCOMUtil.runRecordsetQuery(con, "Select ItemCode from dbo.OITM");
            List<String> itemcodes = new LinkedList<>();
            IItems is = SBOCOMUtil.newItems(con);
            
            int anz_rows = 0;
            int count = rs.getFields().getCount().intValue(); //Anzahl der Spalten der Tabelle OCRD
            String FldVal;
            String FldName;

            while (rs.isEoF().equals(false)) {
                FldName = rs.getFields().item(0).getName();
                FldVal = String.valueOf(rs.getFields().item(0).getValue());
                itemcodes.add(FldVal);
                //Näcster Datensatz
                rs.moveNext();
            }

            System.out.println("GRÖSSE OITM" +  itemcodes.size());
            for (int i = 0; i <  itemcodes.size(); i++) {
                is = SBOCOMUtil.getItems(con,  itemcodes.get(i));
                retVal.add(is);
                
                System.out.println(is.getItemCode());
                System.out.println(is.getItemName());
                System.out.println(i);
            }

        } catch (SBOCOMException ex) {

            System.out.println("Fehler in KLASSE: OITMDAO");
            System.out.println("in FUNKTION: findAll");
            System.out.println(ex.getMessage());
        }

        return retVal;

    }

    public void updateOSCL(IItems is) {
        is.update();
    }

    public void removeOSCL(IItems is) {
        is.remove();
    }

    public void insertOSCL(IItems is) {
        is.add();
    }

}

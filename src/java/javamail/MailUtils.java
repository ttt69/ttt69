/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamail;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author efI
 */
public class MailUtils {

    private MailUtils() {
        
    }
//    
//    public static Properties getPropertyFile(){
//        
//        
//    }

    public static Session getGMailSession(String user, String pass) {

        final Properties props = new Properties();

        Properties prop_MailSession = new Properties();
        //Aus der Property Datei lesen
        InputStream in = MailUtils.class.getResourceAsStream("/properties/Mail_SessionSettings.properties");
        
        try {
            prop_MailSession.load(in);
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // To receive
        props.setProperty("mail.pop3.host", prop_MailSession.getProperty("Rec_host"));
        props.setProperty("mail.pop3.user", user);
        props.setProperty("mail.pop3.password", pass);
        props.setProperty("mail.pop3.port", prop_MailSession.getProperty("Rec_port"));
        props.setProperty("mail.pop3.auth", prop_MailSession.getProperty("Rec_auth"));
        props.setProperty("mail.pop3.socketFactory.class",
                prop_MailSession.getProperty("Rec_socketFactoryClass"));

        // To send
        props.setProperty("mail.smtp.host", prop_MailSession.getProperty("Send_host"));
        props.setProperty("mail.smtp.auth", prop_MailSession.getProperty("Send_auth"));
        props.setProperty("mail.smtp.port", prop_MailSession.getProperty("Send_port"));
        props.setProperty("mail.smtp.socketFactory.port", prop_MailSession.getProperty("Send_socketFactoryPort"));
        props.setProperty("mail.smtp.socketFactory.class",
                prop_MailSession.getProperty("Send_socketFactoryClass"));
        props.setProperty("mail.smtp.socketFactory.fallback",
                prop_MailSession.getProperty("Send_socketFactoryFallback"));

        return Session.getInstance(props, new javax.mail.Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(props.getProperty("mail.pop3.user"),
                        props.getProperty("mail.pop3.password"));

            }
        });
    }

    public static Folder openPop3InboxReadOnly(Session session) throws MessagingException {

        Store store = session.getStore("pop3");
        store.connect();

        Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_WRITE);

        return folder;
    }

    public static void closeInbox(Folder folder) {
        try {
            folder.close(false);
            folder.getStore().close();
        } catch (MessagingException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void postMail(Session session, String recipient, String subject, String message) {

        try {
            Message msg = new MimeMessage(session);

            InternetAddress addressTo = new InternetAddress(recipient);
            msg.setRecipient(Message.RecipientType.TO, addressTo);

            msg.setSubject(subject);
            msg.setContent(message, "text/plain");
            Transport.send(msg);
            System.out.println("Nachricht erfolgreich versendet!");
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OCPR implements Serializable{
    
    private String CntctCode;
    private String CardCode;
    private String Name;
    private String Position;
    private String Address;
    private String Tel1;
    private String E_MailL;

    public OCPR() {
    }

    public OCPR(String CntctCode, String CardCode, String Name, String Position, String Address, String Tel1, String E_MailL) {
        this.CntctCode = CntctCode;
        this.CardCode = CardCode;
        this.Name = Name;
        this.Position = Position;
        this.Address = Address;
        this.Tel1 = Tel1;
        this.E_MailL = E_MailL;
    }

    public String getCntctCode() {
        return CntctCode;
    }

    public void setCntctCode(String CntctCode) {
        this.CntctCode = CntctCode;
    }

    public String getCardCode() {
        return CardCode;
    }

    public void setCardCode(String CardCode) {
        this.CardCode = CardCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String Position) {
        this.Position = Position;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getTel1() {
        return Tel1;
    }

    public void setTel1(String Tel1) {
        this.Tel1 = Tel1;
    }

    public String getE_MailL() {
        return E_MailL;
    }

    public void setE_MailL(String E_MailL) {
        this.E_MailL = E_MailL;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj!=null && obj instanceof OCPR){
            
            OCPR o=(OCPR) obj;
            
            if(this.CntctCode.equals(o.CntctCode)&&this.CardCode.equals(o.CardCode)){
                ret=true;
            }
        }
        
        
        
        return ret;
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.CntctCode);
        hash = 67 * hash + Objects.hashCode(this.CardCode);
        hash = 67 * hash + Objects.hashCode(this.Name);
        hash = 67 * hash + Objects.hashCode(this.Position);
        hash = 67 * hash + Objects.hashCode(this.Address);
        hash = 67 * hash + Objects.hashCode(this.Tel1);
        hash = 67 * hash + Objects.hashCode(this.E_MailL);
        return hash;
    }
    
    
    
    
    
}

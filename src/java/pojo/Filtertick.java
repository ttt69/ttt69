/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patrick
 */
public class Filtertick implements Serializable{
     private List<Ticket> filterTickets =  new ArrayList<>();

    public Filtertick() {
    }
    
    public Filtertick(ArrayList<Ticket> tick){
        filterTickets = tick;
    }

    public List<Ticket> getFilterTickets() {
        return filterTickets;
    }

    public void setFilterTickets(List<Ticket> filterTickets) {
        this.filterTickets = filterTickets;
    }
     
     
}
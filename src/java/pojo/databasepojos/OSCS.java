/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;

/**
 *
 * @author Florian
 */
public class OSCS implements Serializable{
    private String statusID;
    private String Name;
    private String Descriptio;

    public OSCS() {
    }

    public OSCS(String statusID, String Name, String Descriptio) {
        this.statusID = statusID;
        this.Name = Name;
        this.Descriptio = Descriptio;
    }

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescriptio() {
        return Descriptio;
    }

    public void setDescriptio(String Descriptio) {
        this.Descriptio = Descriptio;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
    
        if(obj!=null && obj instanceof OSCS){
            OSCS o=(OSCS) obj;
            
            if(this.statusID.equals(o.statusID) && this.Descriptio.equals(o.Descriptio) && this.Name.equals(o.Name)){
                ret=true;
            }
        }
    
        return ret;
    }
    
    
}

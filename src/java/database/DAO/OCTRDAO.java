/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database.DAO;

import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.IServiceContracts;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Florian
 */
public class OCTRDAO {

    public OCTRDAO() {
    }
    
    public List<IServiceContracts> findAll(){

        List<IServiceContracts> retVal = new LinkedList<>(); //ACHTUNG INDEX VON LINKED LIST BEGINNT BEI 0
        ICompany con = ConnectionManager.getInst().getConn();
        try {
            IRecordset rs = SBOCOMUtil.runRecordsetQuery(con, "Select COUNT(*) from dbo.OCTR");
            IServiceContracts sc=SBOCOMUtil.newServiceContracts(con);
            
            
            int anz_rows=0;
            int count = rs.getFields().getCount().intValue();
            
            while (rs.isEoF().equals(false)) {
                System.out.println(rs.getFields().item(0).getValue());
                //Integer.parseInt((String) rs.getFields().item(0).getValue());
                anz_rows = (int) rs.getFields().item(0).getValue();
                System.out.println("CHECKVAL");
                System.out.println("FldVal = " + anz_rows);
            //Move to the next record
            rs.moveNext();
            }
            
            
            for(int i=1;i<=anz_rows;i++){
                
                sc=SBOCOMUtil.getServiceContracts(con, i);
                System.out.println(sc.getCustomerCode());
                System.out.println(sc.getCustomerName());
                System.out.println(sc.getContractID());
                retVal.add(sc);
            }
            
        } catch (SBOCOMException ex) {
           
            System.out.println("Fehler in KLASSE: OCTRDAO");
            System.out.println("in FUNKTION: findAll");
            System.out.println(ex.getMessage());
        }
    

       

        return retVal;
    }
    
    public void updateOSCL(IServiceContracts is){
        is.update();
    }
    public void removeOSCL(IServiceContracts is){
        is.remove();
    }
    
    public void insertOSCL(IServiceContracts is){
        is.add();
    }
    
}

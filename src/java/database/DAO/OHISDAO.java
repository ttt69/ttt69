/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Florian
 */
public class OHISDAO {
    //History for Ticket

    public OHISDAO() {
    }

    public List<OHIS> findAll() {

        List<OHIS> retVal = new LinkedList<>(); //ACHTUNG INDEX VON LINKED LIST BEGINNT BEI 0
        ICompany con = ConnectionManager.getInst().getConn();
        try {

            IRecordset rs = SBOCOMUtil.runRecordsetQuery(con, "Select * from [dbo].[@OHIS]");
            OHIS o = new OHIS();

            int anz_rows = 0;
            int count = rs.getFields().getCount().intValue(); //Anzahl der Spalten der Tabelle
            String FldVal;
            String FldName;

            while (rs.isEoF().equals(false)) {
                o = new OHIS();

                for (int i = 0; i < count; i++) { //Ermitteln der Werte der einzelnen Spalten pro Zeile
                    FldName = rs.getFields().item(i).getName();
                    FldVal = String.valueOf(rs.getFields().item(i).getValue());

                    switch (i) {
                        case 0:
                            o.setCode(FldVal);
                            break;

                        case 1:
                            o.setName(FldVal);
                            break;

                        case 2:
                            o.setU_date(FldVal);
                            break;

                        case 3:
                            o.setU_user(Integer.valueOf(FldVal));
                            break;

                        case 4:
                            o.setU_description(FldVal);
                            break;

                        case 5:
                            o.setU_time(Double.valueOf(FldVal));
                            break;

                        case 6:
                            o.setU_ticketnummer(Integer.valueOf(FldVal));
                            break;

                    }
                }

                retVal.add(o);

                System.out.println(o);

                //Move to the next record
                rs.moveNext();
            }

        } catch (SBOCOMException ex) {

            System.out.println("Fehler in KLASSE: OHISDAO");
            System.out.println("in FUNKTION: findAll");
            System.out.println(ex.getMessage());
        }
        return retVal;

    }

    public void updateOHIS(OHIS o) {
        ICompany con = ConnectionManager.getInst().getConn();
        
        
        String sqlcmd="UPDATE [dbo].[@OHIS] SET Code='"+o.getCode()+"',Name='"+o.getName()+"',U_date='"+o.getU_date()+"',U_user="+o.getU_user()+",U_description='"+o.getU_description()+"', U_time="+o.getU_time()+",U_ticketnummer="+o.getU_ticketnummer()+" where Code='"+o.getCode()+"'";
        
        try {
            SBOCOMUtil.runRecordsetQuery(con,sqlcmd);
            
        } catch (SBOCOMException ex) {
            System.out.println("Fehler in OHIS DAO");
            System.out.println(ex.getCause());
            System.out.println(ex.getMessage());
        }
    }

    public void removeOHIS(String o) {
         ICompany con = ConnectionManager.getInst().getConn();
         
        try {
            
            SBOCOMUtil.runRecordsetQuery(con,"Delete from [dbo].[@OHIS] where Code='"+o+"'");
            
            
            
        } catch (SBOCOMException ex) {
            System.out.println("Fehler in KLASSE: OHISDAO");
            System.out.println("in FUNKTION: removeOHIS");
            System.out.println(ex.getMessage());
        }
         
    }

    public void insertOHIS(OHIS o) {
         String sqlcmd="Insert into [dbo].[@OHIS] VALUES('"+o.getCode()+"','"+o.getName()+"','"+o.getU_date()+"',"+o.getU_user()+",'"+o.getU_description()+"',"+o.getU_time()+","+o.getU_ticketnummer()+")";
          ICompany con = ConnectionManager.getInst().getConn();
          
      try {
            SBOCOMUtil.runRecordsetQuery(con,sqlcmd);
            
        } catch (SBOCOMException ex) {
            System.out.println("Fehler in OHIS DAO");
            System.out.println("IN FUNCTION INSERTOHIS");
            System.out.println(ex.getCause());
            System.out.println(ex.getMessage());
        }
    
    
    
    
    }

}

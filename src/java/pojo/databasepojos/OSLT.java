/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

/**
 *
 * @author Florian
 */
public class OSLT {
    private String SltCode;
    private String ItemCode;
    private String StatusNum;
    private String DateCreate;
    private String DateUpdate;
    private String Subject;
    private String Symptom;
    private String Cause;
    private String Descriptio;

    public OSLT() {
    }

    public OSLT(String SltCode, String ItemCode, String StatusNum, String DateCreate, String DateUpdate, String Subject, String Symptom, String Cause, String Descriptio) {
        this.SltCode = SltCode;
        this.ItemCode = ItemCode;
        this.StatusNum = StatusNum;
        this.DateCreate = DateCreate;
        this.DateUpdate = DateUpdate;
        this.Subject = Subject;
        this.Symptom = Symptom;
        this.Cause = Cause;
        this.Descriptio = Descriptio;
    }

    public String getSltCode() {
        return SltCode;
    }

    public void setSltCode(String SltCode) {
        this.SltCode = SltCode;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String ItemCode) {
        this.ItemCode = ItemCode;
    }

    public String getStatusNum() {
        return StatusNum;
    }

    public void setStatusNum(String StatusNum) {
        this.StatusNum = StatusNum;
    }

    public String getDateCreate() {
        return DateCreate;
    }

    public void setDateCreate(String DateCreate) {
        this.DateCreate = DateCreate;
    }

    public String getDateUpdate() {
        return DateUpdate;
    }

    public void setDateUpdate(String DateUpdate) {
        this.DateUpdate = DateUpdate;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String Subject) {
        this.Subject = Subject;
    }

    public String getSymptom() {
        return Symptom;
    }

    public void setSymptom(String Symptom) {
        this.Symptom = Symptom;
    }

    public String getCause() {
        return Cause;
    }

    public void setCause(String Cause) {
        this.Cause = Cause;
    }

    public String getDescriptio() {
        return Descriptio;
    }

    public void setDescriptio(String Descriptio) {
        this.Descriptio = Descriptio;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj !=null && obj instanceof OSLT){
            OSLT o=(OSLT) obj;
            if(this.SltCode.equals(o.SltCode) && this.ItemCode.equals(o.ItemCode)){
                ret=true;
            }
            
        }
        
        return ret;
    }
    
    
    
    
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.faces.context.FacesContext;
import pojo.databasepojos.OSCL;
import user.User;
import user.Users;

/**
 *
 * @author efI
 */
public class LoginData implements Serializable{

    private String username = "";
    private String password = "";
    private User loggedUser;
    public List<OSCL> l = new ArrayList<>();

    FacesContext context = FacesContext.getCurrentInstance();
    Users users = (Users) context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);

    public LoginData() {

//        List<OCPR> l1=OCPRDAO.initOCPR();
//        List<OCRD> l2=OCRDDAO.initOCRD();
//        
//        
//        OCPRDAO.insertOCPR(l2.get(3),l1.get(3));
        
        User u = new User(7, "Efraim", "", "Efraim", "Guias", "Mitarbeiter");
        users.addUser(u);
        u = new User(8, "Florian", "", "Florian", "Peischl", "Mitarbeiter");
        users.addUser(u);
        u = new User(9, "Patrick", "", "Patrick", "Kopper", "Mitarbeiter");
        users.addUser(u);
    }

    public String doLogin() {

        if (username.equals("admin") && password.equals("admin")) {
            return "admin";
        } else {
            for(User user: users.getUsers()){
                if (username.equals(user.getUsername()) && password.equals(user.getPassword())){
                    loggedUser = user;
                    return "success";
                }
            }
            return "failure";
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }
  
}

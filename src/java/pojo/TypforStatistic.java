/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

/**
 *
 * @author efI
 */
public class TypforStatistic {
    
    private int year;
    private int month;
    private int open;
    private int inOperation;
    private int closed;

    public TypforStatistic() {
        year = 0;
        month = 0;
        open = 0;
        inOperation = 0;
        closed = 0;
    }
    
    
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int open) {
        this.open = open;
    }

    public int getInOperation() {
        return inOperation;
    }

    public void setInOperation(int inOperation) {
        this.inOperation = inOperation;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    @Override
    public String toString() {
        return year + "/" + month + ": " + open + "," + inOperation + "," + closed;
    }
    
    

    
}

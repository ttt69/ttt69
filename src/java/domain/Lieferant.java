package domain;

/**
 *
 * @author Snowpro
 */
public class Lieferant {

    private String name;
    private Adresse lief_adress;

    public Lieferant(String name, Adresse lief_adress) {
        this.name = name;
        this.lief_adress = lief_adress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Adresse getLief_adress() {
        return lief_adress;
    }

    public void setLief_adress(Adresse lief_adress) {
        this.lief_adress = lief_adress;
    }

}

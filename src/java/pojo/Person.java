/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Patrick
 */
public class Person implements Serializable{
    
    private int id;
    private String firstname = "";
    private String surname = "";
    private String phone = "";
    private String email = "";
    private String firmenid = "";

    public Person() {
    }

    public Person(int id, String vorname, String nachname) {
        this.id = id;
        this.firstname = vorname;
        this.surname = nachname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirmenid() {
        return firmenid;
    }

    public void setFirmenid(String firmenid) {
        this.firmenid = firmenid;
    }
    
    
    @Override
    public String toString() {
        return firstname + " " + surname;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.id;
        hash = 73 * hash + Objects.hashCode(this.firstname);
        hash = 73 * hash + Objects.hashCode(this.surname);
        hash = 73 * hash + Objects.hashCode(this.phone);
        hash = 73 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if(obj != null){
            if(obj instanceof Person){
                Person person = (Person) obj;
                
                if(person.getFirstname().equals(this.firstname)){
                    if(person.getSurname().equals(this.surname)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
}

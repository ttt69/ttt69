/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OITM implements Serializable {

    private String ItemCode;
    private String ItemName;
    private String ItmsGrpCod;

    public OITM() {
    }

    public OITM(String ItemCode, String ItemName, String ItmsGrpCod) {
        this.ItemCode = ItemCode;
        this.ItemName = ItemName;
        this.ItmsGrpCod = ItmsGrpCod;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String ItemCode) {
        this.ItemCode = ItemCode;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

    public String getItmsGrpCod() {
        return ItmsGrpCod;
    }

    public void setItmsGrpCod(String ItmsGrpCod) {
        this.ItmsGrpCod = ItmsGrpCod;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret = false;

        if (obj != null && obj instanceof OITM) {
            OITM o = (OITM) obj;
            if (this.ItemCode.equals(o.ItemCode)) {
                ret = true;
            }
        }

        return ret;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.ItemCode);
        hash = 19 * hash + Objects.hashCode(this.ItemName);
        hash = 19 * hash + Objects.hashCode(this.ItmsGrpCod);
        return hash;
    }
    
    

}

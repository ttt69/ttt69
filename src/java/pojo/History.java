/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.io.Serializable;
import java.util.Objects;
import user.User;

/**
 *
 * @author efI
 */
public class History implements Serializable{

    
    public String date;
    public User user;
    public String description;
    public float time;
    int ticketnummer;
    /**
     * Creates a new instance of History
     */
    public History() {
    }

    public History(String date, User user, String beschreibung, float zeit, float gesamtzeit) {
        this.date = date;
        this.user = user;
        this.description = beschreibung;
        this.time = zeit;
    }

    public History(String date, User user, float time, float total_time) {
        this.date = date;
        this.user = user;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "History{" + "date=" + date + ", user=" + user + ", beschreibung=" + description + ", zeit=" + time + '}';
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public int getTicketnummer() {
        return ticketnummer;
    }

    public void setTicketnummer(int ticketnummer) {
        this.ticketnummer = ticketnummer;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.date);
        hash = 89 * hash + Objects.hashCode(this.user);
        hash = 89 * hash + Objects.hashCode(this.description);
        hash = 89 * hash + Float.floatToIntBits(this.time);
        hash = 89 * hash + this.ticketnummer;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        
        if(obj != null){
            if(obj instanceof History){
                History p = (History) obj;
                
                if(p.getTicketnummer() == this.getTicketnummer()){
                    return true;
                }
            }
        }
        return false;
    }
}

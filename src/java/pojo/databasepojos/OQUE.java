/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OQUE  implements Serializable{
    private String queueID;
    private String descript;
    private String manager;

    public OQUE() {
    }

    public OQUE(String queueID, String descript, String manager) {
        this.queueID = queueID;
        this.descript = descript;
        this.manager = manager;
    }

    public String getQueueID() {
        return queueID;
    }

    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj != null && obj instanceof OQUE){
            OQUE o=(OQUE) obj;
            if(this.queueID.equals(o.queueID)){
                ret=true;
            }
            
        }
        
        
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.queueID);
        hash = 83 * hash + Objects.hashCode(this.descript);
        hash = 83 * hash + Objects.hashCode(this.manager);
        return hash;
    }
    
    
    
    
    
    
}

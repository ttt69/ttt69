/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.Serializable;
import pojo.*;

/**
 *
 * @author efI
 */
public class Settings implements Serializable {

    private Priority defaultPrio = Priority.WICHTIG;
    private Typ defaultTyp = Typ.Auftrag_NV;
    private State defaultStatus = State.OFFEN;

    private String defaultLeistung;
    private String defaultLösungsm;
    private boolean checkbox = true;

    public Priority getDefaultPrio() {
        return defaultPrio;
    }

    public void setDefaultPrio(Priority defaultPrio) {
        this.defaultPrio = defaultPrio;
    }

    public Typ getDefaultTyp() {
        return defaultTyp;
    }

    public void setDefaultTyp(Typ defaultTyp) {
        this.defaultTyp = defaultTyp;
    }

    public State getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(State defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public String getDefaultLeistung() {
        return defaultLeistung;
    }

    public void setDefaultLeistung(String defaultLeistung) {
        this.defaultLeistung = defaultLeistung;
    }

    public String getDefaultLösungsm() {
        return defaultLösungsm;
    }

    public void setDefaultLösungsm(String defaultLösungsm) {
        this.defaultLösungsm = defaultLösungsm;
    }

    public String checkbox() {
        String ret = "";
        if (checkbox == true) {
            ret = "_blank";
        }
        return ret;
    }

    public boolean isCheckbox() {
        return checkbox;
    }

    public void setCheckbox(boolean checkbox) {
        this.checkbox = checkbox;
    }

}

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package pojotodao;
//
//import java.sql.Date;
//import java.util.ArrayList;
//import java.util.List;
//import pojo.Kunde;
//import pojo.Person;
//import pojo.Ticket;
//import pojo.databasepojos.OCPR;
//import pojo.databasepojos.OCRD;
//import pojo.databasepojos.OSCL;
//
///**
// *
// * @author Patrick
// */
//public class ConvertToDAO {
//    
//    public static void writetoDB(List<Ticket> tickets, List<Kunde> kunden, List<Person> personen){
//        List<OSCL> oscllist = new ArrayList<>();
//        List<OCRD> ocrdlist = new ArrayList<>();
//        List<OCPR> ocprlist = new ArrayList<>();
//        OSCL oscl = null;
//        OCRD ocrd = null;
//        OCPR ocpr = null;
//        
//        for(Ticket t:tickets){
//            oscl = new OSCL();
//            ocrd = new OCRD();
//            ocpr = new OCPR();
//            
//            
//            oscl.setAssignee(t.zuständiger);
//            oscl.setCallID(t.ticketnummer+"");
//            oscl.setCloseDate((Date) t.endedat);
//            
//            oscl.setContctCode(t.ansprechpartner.getId()+"");
//            oscl.setCreateDate((Date) t.startdat);
//            
//            
//            oscl.setCustmrName(t.kundenname);
//            oscl.setCustomer(t.kundennummer+"");
//            oscl.setDescrption(t.beschreibung);
//            oscl.setPriority(t.priorität.getName());
//            oscl.setStatus(t.status.getName());
//            
//            
//        }
//        
//        for(Kunde k:kunden){
//            ocrd = new OCRD();
//            ocrd.setCardCode("C" +k.getNummer());
//            ocrd.setCardName(k.getName());
//        }
//        
//        for(Person p:personen){
//            ocpr = new OCPR();
//            ocpr.setName(p.getVorname()+ " "+ p.getNachname());
//            ocpr.setCntctCode(p.getFirmenid());
//            ocpr.setTel1(p.getTelefon());
//            ocpr.setE_MailL(p.getEmail());
//        }
//    }
//    
//}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo.databasepojos;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OSCL implements Serializable {

    private String callID;          //NOT-NULL
    private String subject;
    private String customer;
    private String custmrName;
    private String contctCode;
    private String status;
    private String priority;
    private String descrption;
    private Date createDate;
    private String createTime;
    private Date closeDate;
    private String closeTime;
    private Date resolDate;
    private String resolTime;
    private String Instance;        //NOT-NULL
    private String DocNum;          //NOT-NULL
    private String PIndicator;      //NOT-NULL
    private String assignee;        //Zuständiger
    private String respAssign;      //Entgegengenommen von diesem USER

    public OSCL(String callID, String subject, String customer, String custmrName, String contctCode, String status, String priority, String descrption, Date createDate, String createTime, Date closeDate, String closeTime, Date resolDate, String resolTime, String Instance, String DocNum, String PIndicator,String assignee,String respAssign) {
        this.callID = callID;
        this.subject = subject;
        this.customer = customer;
        this.custmrName = custmrName;
        this.contctCode = contctCode;
        this.status = status;
        this.priority = priority;
        this.descrption = descrption;
        this.createDate = createDate;
        this.createTime = createTime;
        this.closeDate = closeDate;
        this.closeTime = closeTime;
        this.resolDate = resolDate;
        this.resolTime = resolTime;
        this.Instance = Instance;
        this.DocNum = DocNum;
        this.PIndicator = PIndicator;
        this.assignee=assignee;
        this.respAssign=respAssign;
    }
    
    
    

    public OSCL() {
    }

    @Override
    public String toString() {
        return "OSCL{" + "callID=" + callID + ", subject=" + subject + ", customer=" + customer + ", custmrName=" + custmrName + ", contctCode=" + contctCode + ", status=" + status + ", priority=" + priority + ", descrption=" + descrption + ", createDate=" + createDate + ", createTime=" + createTime + ", closeDate=" + closeDate + ", closeTime=" + closeTime + ", resolDate=" + resolDate + ", resolTime=" + resolTime + ", Instance=" + Instance + ", DocNum=" + DocNum + ", PIndicator=" + PIndicator + ", assignee=" + assignee + ", respAssign=" + respAssign + '}';
    }

    

   

   

    
    
    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj != null){
            OSCL o=(OSCL) obj;
            
            if(o instanceof OSCL){
                ret=true;
            }
        }
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.callID);
        hash = 59 * hash + Objects.hashCode(this.subject);
        hash = 59 * hash + Objects.hashCode(this.customer);
        hash = 59 * hash + Objects.hashCode(this.custmrName);
        hash = 59 * hash + Objects.hashCode(this.contctCode);
        hash = 59 * hash + Objects.hashCode(this.status);
        hash = 59 * hash + Objects.hashCode(this.priority);
        hash = 59 * hash + Objects.hashCode(this.descrption);
        return hash;
    }

    public String getInstance() {
        return Instance;
    }

    public void setInstance(String Instance) {
        this.Instance = Instance;
    }

    public String getDocNum() {
        return DocNum;
    }

    public void setDocNum(String DocNum) {
        this.DocNum = DocNum;
    }

    public String getPIndicator() {
        return PIndicator;
    }

    public void setPIndicator(String PIndicator) {
        this.PIndicator = PIndicator;
    }

    
    
    
    
    
    
    public String getCallID() {
        return callID;
    }

    public void setCallID(String callID) {
        this.callID = callID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustmrName() {
        return custmrName;
    }

    public void setCustmrName(String custmrName) {
        this.custmrName = custmrName;
    }

    public String getContctCode() {
        return contctCode;
    }

    public void setContctCode(String contctCode) {
        this.contctCode = contctCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescrption() {
        return descrption;
    }

    public void setDescrption(String descrption) {
        this.descrption = descrption;
    }



    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }



    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }



    public String getResolTime() {
        return resolTime;
    }

    public void setResolTime(String resolTime) {
        this.resolTime = resolTime;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Date getResolDate() {
        return resolDate;
    }

    public void setResolDate(Date resolDate) {
        this.resolDate = resolDate;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getRespAssign() {
        return respAssign;
    }

    public void setRespAssign(String respAssign) {
        this.respAssign = respAssign;
    }
    
    
          
    
    
    
}

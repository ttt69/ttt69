/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.SBOCOMException;
import com.sap.smb.sbo.api.SBOCOMUtil;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Florian
 */
public class TTT_TUSRDAO {

    public List<TTT_TUSR> findAll() {

        List<TTT_TUSR> retVal = new LinkedList<>(); //ACHTUNG INDEX VON LINKED LIST BEGINNT BEI 0
        ICompany con = ConnectionManager.getInst().getConn();
        try {

            IRecordset rs = SBOCOMUtil.runRecordsetQuery(con, "Select * from [dbo].[@TTT_TUSR]");
            TTT_TUSR o = new TTT_TUSR();

            int anz_rows = 0;
            int count = rs.getFields().getCount().intValue(); //Anzahl der Spalten der Tabelle
            String FldVal;
            String FldName;

            while (rs.isEoF().equals(false)) {
                o = new TTT_TUSR();

                for (int i = 0; i < count; i++) { //Ermitteln der Werte der einzelnen Spalten pro Zeile
                    FldName = rs.getFields().item(i).getName();
                    FldVal = String.valueOf(rs.getFields().item(i).getValue());

                    switch (i) {
                        case 0:
                            o.setCode(FldVal);
                            break;

                        case 1:
                            o.setName(FldVal);
                            break;

                        case 2:
                            o.setU_Password(FldVal);
                            break;

                        case 3:
                            o.setU_Role(FldVal);
                            break;

                    }
                }

                retVal.add(o);

                System.out.println(o);

                //Move to the next record
                rs.moveNext();
            }

        } catch (SBOCOMException ex) {

            System.out.println("Fehler in KLASSE: OHISDAO");
            System.out.println("in FUNKTION: findAll");
            System.out.println(ex.getMessage());
        }
        return retVal;

    }

    public void updateTTT_TUSR(TTT_TUSR o) {

        ICompany con = ConnectionManager.getInst().getConn();

        String sqlcmd = "UPDATE [dbo].[@TTT_TUSR] SET Code='"+o.getCode()+"',Name='"+o.getName()+"',"
                + "U_Password='"+o.getU_Password()+"',U_Role='"+o.getU_Role()+"' where Code='"+o.getCode()+"'";
        
        try {

            SBOCOMUtil.runRecordsetQuery(con, sqlcmd);

        } catch (SBOCOMException ex) {
            System.out.println("Fehler in KLASSE: TTUSR_DAO");
            System.out.println("in FUNKTION: updateTTT_TUSR");
            System.out.println(ex.getMessage());
        }

    }

    public void removeTTT_TUSR(String o) {
        ICompany con = ConnectionManager.getInst().getConn();

        try {

            SBOCOMUtil.runRecordsetQuery(con, "Delete from [dbo].[@TTT_TUSR] where Code='" + o + "'");

        } catch (SBOCOMException ex) {
            System.out.println("Fehler in KLASSE: TTUSR_DAO");
            System.out.println("in FUNKTION: removeTTT_TUSR");
            System.out.println(ex.getMessage());
        }

    }

    //NOCH ZU TESTEN!!!
    public void insertTTT_TUSR(TTT_TUSR o) {
        
        
          ICompany con = ConnectionManager.getInst().getConn();
          
         String sqlcmd= "Insert into [dbo].[@TTT_TUSR] VALUES('"+o.getCode()+"','"+o.getName()+"','"+o.getU_Password()+"','"+o.getU_Role()+"')";

        try {
            SBOCOMUtil.runRecordsetQuery(con, sqlcmd);
        } catch (SBOCOMException ex) {
               System.out.println("Fehler in KLASSE: TTUSR_DAO");
            System.out.println("in FUNKTION: insertTTT_TUSR");
            System.out.println(ex.getMessage());
        }
        
    }

}

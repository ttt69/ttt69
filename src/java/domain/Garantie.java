package domain;

/**
 *
 * @author Snowpro
 */
public class Garantie {

    private String art;
    private String dauer;

    public Garantie(String art, String dauer) {
        this.art = art;
        this.dauer = dauer;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String getDauer() {
        return dauer;
    }

    public void setDauer(String dauer) {
        this.dauer = dauer;
    }

}

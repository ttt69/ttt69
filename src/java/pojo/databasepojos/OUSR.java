/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OUSR implements Serializable{
    
    private String USERID;
    private String USER_CODE;
    private String U_NAME;
    private String E_Mail;

    public OUSR() {
    }

    public OUSR(String USERID, String USER_CODE, String U_NAME, String E_Mail) {
        this.USERID = USERID;
        this.USER_CODE = USER_CODE;
        this.U_NAME = U_NAME;
        this.E_Mail = E_Mail;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj!=null && obj instanceof OUSR){
            OUSR o=(OUSR) obj;
            
            if(this.USER_CODE.equals(o.USER_CODE) && this.USERID.equals(o.USERID)){
                ret=true;
            }
        }
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.USERID);
        hash = 83 * hash + Objects.hashCode(this.USER_CODE);
        return hash;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getUSER_CODE() {
        return USER_CODE;
    }

    public void setUSER_CODE(String USER_CODE) {
        this.USER_CODE = USER_CODE;
    }

    public String getU_NAME() {
        return U_NAME;
    }

    public void setU_NAME(String U_NAME) {
        this.U_NAME = U_NAME;
    }

    public String getE_Mail() {
        return E_Mail;
    }

    public void setE_Mail(String E_Mail) {
        this.E_Mail = E_Mail;
    }
    
    
    
}

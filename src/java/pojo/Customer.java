/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Patrick
 */
public class Customer implements Serializable {

    private Person person;
    private String cnr;
    private String cname;
    private String product;
    private int time;
    private List<Person> contactperson = new ArrayList<>();
    private String address;

    public Customer() {
    }

    public Customer(Person person, String cnr, String cname, String product, int time, String address) {
        this.person = person;
        this.cnr = cnr;
        this.cname = cname;
        this.product = product;
        this.time = time;
        this.address = address;
    }

    public String getCnr() {
        return cnr;
    }

    public void setCnr(String cnr) {
        this.cnr = cnr;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public List<Person> getContactperson() {
        return contactperson;
    }

    public void setContactperson(List<Person> contactperson) {
        this.contactperson = contactperson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.cnr);
        hash = 59 * hash + Objects.hashCode(this.cname);
        hash = 59 * hash + Objects.hashCode(this.product);
        hash = 59 * hash + this.time;
        hash = 59 * hash + Objects.hashCode(this.contactperson);
        hash = 59 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj != null) {
            if (obj instanceof Customer) {
                Customer kunde = (Customer) obj;

                if (kunde.getCnr().equals(this.cnr)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return cnr + " " + cname;
    }
}

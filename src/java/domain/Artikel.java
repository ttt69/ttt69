package domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Snowpro
 */
public class Artikel {

    private String name;
    private String artikel_nr;
    private List<Serial> serial_list = new ArrayList<Serial>();

    public Artikel(String name, String artikel_nr, List<Serial> serial_list) {
        this.name = name;
        this.artikel_nr = artikel_nr;
        this.serial_list = serial_list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtikel_nr() {
        return artikel_nr;
    }

    public void setArtikel_nr(String artikel_nr) {
        this.artikel_nr = artikel_nr;
    }

    public List<Serial> getSerial_list() {
        return serial_list;
    }

    public void setSerial_list(List<Serial> serial_list) {
        this.serial_list = serial_list;
    }

    @Override
    public String toString() {
        return name;
    }

}

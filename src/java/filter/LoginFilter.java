/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import bean.LoginBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alex
 */
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        LoginBean loginBean = (LoginBean) request.getSession().getAttribute("loginBean");
        RequestDispatcher toLogin = request.getRequestDispatcher("/startPage.xhtml");
        RequestDispatcher toError = request.getRequestDispatcher("/error.xhtml");

        List blacklist_kunde = new ArrayList();
        blacklist_kunde.add("admin.xhtml");
        blacklist_kunde.add("filter_history.xhtml");
        blacklist_kunde.add("filter_hardware.xhtml");
        blacklist_kunde.add("filter_myView.xhtml");
        blacklist_kunde.add("filter_open.xhtml");
        blacklist_kunde.add("filter_software.xhtml");
        blacklist_kunde.add("filter_overview.xhtml");
        blacklist_kunde.add("index_rma.xhtml");
        blacklist_kunde.add("newTicket.xhtml");
        blacklist_kunde.add("statistic.xhtml");
        blacklist_kunde.add("settings.xhtml");
        blacklist_kunde.add("statistic_historyperEmp.xhtml");
        blacklist_kunde.add("statistic_perEmp.xhtml");
        
        List blacklist_ma = new ArrayList();
        blacklist_ma.add("admin.xhtml");
        blacklist_ma.add("filter_hardware.xhtml");
        blacklist_ma.add("filter_history.xhtml");
        blacklist_ma.add("filter_myView.xhtml");
        blacklist_ma.add("filter_myView_kunde.xhtml");
        blacklist_ma.add("filter_open.xhtml");
        blacklist_ma.add("filter_overview.xhtml");
        blacklist_ma.add("filter_software.xhtml");
        blacklist_ma.add("settings.xhtml");
        blacklist_ma.add("statistic.xhtml");
        blacklist_ma.add("statistic_perEmp.xhtml");

        

        String url = request.getRequestURI().toString();
        String split[] = url.split("/");
        String check = "";
        if (split.length == 3) {
            check = split[2];
            System.out.println("URL: " + check);
        }

        if (loginBean == null || !loginBean.isLoggedIn()) {
            toLogin.forward(request, response);

            //response.sendRedirect(request.getContextPath() + "/startPage.xhtml"); // No logged-in user found, so redirect to login page.
        } else {
            String role = loginBean.getRole();
            System.out.println("Rolle: " + role);
            if (role.equals("Kunde")) {
                for (int i = 0; i < blacklist_kunde.size(); i++) {
                    if (check.equals(blacklist_kunde.get(i))) {
                        toError.forward(request, response);
                    }
                }
            } else {
                if (role.equals("Mitarbeiter")) {
                    for (int i = 0; i < blacklist_ma.size(); i++) {
                        if (check.equals(blacklist_ma.get(i))) {
                            toError.forward(request, response);
                        }
                    }
                }

            }
            chain.doFilter(req, res); // Logged-in user found, so just continue request.
        }
    }

    @Override
    public void destroy() {
        //To change body of generated methods, choose Tools | Templates.
    }

}

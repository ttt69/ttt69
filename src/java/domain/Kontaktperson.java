package domain;

/**
 *
 * @author Snowpro
 */
public class Kontaktperson {

    String name;

    public Kontaktperson(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

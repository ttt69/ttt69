package domain;

/**
 *
 * @author Snowpro
 */
public class Serial {

    private String serial_nr;
    private Garantie garantie;
    private Lieferant liefer;

    public Serial(String serial_nr, Garantie garantie, Lieferant liefer) {
        this.serial_nr = serial_nr;
        this.garantie = garantie;
        this.liefer = liefer;
    }

    public String getSerial_nr() {
        return serial_nr;
    }

    public void setSerial_nr(String serial_nr) {
        this.serial_nr = serial_nr;
    }

    public Garantie getGarantie() {
        return garantie;
    }

    public void setGarantie(Garantie garantie) {
        this.garantie = garantie;
    }

    public Lieferant getLiefer() {
        return liefer;
    }

    public void setLiefer(Lieferant liefer) {
        this.liefer = liefer;
    }


}

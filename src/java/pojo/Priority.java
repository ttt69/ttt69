/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author efI
 */
public class Priority implements Serializable{

    private int id;
    private String name;

    public static final Priority SPERRE = new Priority(1, "Sperre");
    public static final Priority WICHTIG = new Priority(2, "Wichtig");
    public static final Priority W_WICHTIG = new Priority(3, "Weniger Wichtig");
    public static final Priority TRIVIAL = new Priority(4, "Trivial");
    public static final Priority NEUTRAL = new Priority(5, "Neutral");

    public static final List<Priority> priorities = new ArrayList<Priority>() {
        {
            this.add(SPERRE);
            this.add(WICHTIG);
            this.add(W_WICHTIG);
            this.add(TRIVIAL);
            this.add(NEUTRAL);
        }
    };

    public Priority(int i, String name) {
        this.id = i;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Priority> getPriorities() {
        return priorities;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        hash = 89 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if(obj != null){
            if(obj instanceof Priority){
                Priority p = (Priority) obj;
                
                if(p.getId() == this.getId()){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
    }
    

    
    
}

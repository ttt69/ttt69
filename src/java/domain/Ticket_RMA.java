package domain;

public class Ticket_RMA {


    private int ticket_nr;
    private String status;
    private String priori;
    private String auth;
    private Firma firma;
    private Artikel artikel;
    private Serial serial; //enthält Garantie und Lieferant
    
    private String fehler_beschr;
    private String bemerkung;

    public Ticket_RMA(int ticket_nr, String status, String priori, String auth,
                    Firma firma, Artikel artikel, Serial serial,
                    String beschr, String bemerkung) {
        this.ticket_nr = ticket_nr;
        this.status = status;
        this.priori = priori;
        this.auth = auth;
        
        this.firma = firma;
        this.artikel = artikel;
        this.serial = serial;
        
        this.fehler_beschr = beschr;
        this.bemerkung = bemerkung;
        

    }

    public int getTicket_nr() {
        return ticket_nr;
    }

    public void setTicket_nr(int ticket_nr) {
        this.ticket_nr = ticket_nr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriori() {
        return priori;
    }

    public void setPriori(String priori) {
        this.priori = priori;
    }

    public Firma getFirma() {
        return firma;
    }

    public void setFirma(Firma firma) {
        this.firma = firma;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public void setArtikel(Artikel artikel) {
        this.artikel = artikel;
    }

    public Serial getSerial() {
        return serial;
    }

    public void setSerial(Serial serial) {
        this.serial = serial;
    }

    public String getFehler_beschr() {
        return fehler_beschr;
    }

    public void setFehler_beschr(String fehler_beschr) {
        this.fehler_beschr = fehler_beschr;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
    
    
}
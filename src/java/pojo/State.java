/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author efI
 */
public class State implements Serializable{
    private int id;
    private String name;
    
    public static final State OFFEN = new State(1, "offen");
    public static final State IN_ARBEIT = new State(2, "in Arbeit");
    public static final State GESCHLOSSEN = new State(3, "geschlossen");
            
    public static final List<State> statusList = new ArrayList<State>(){
        {
            this.add(OFFEN);
            this.add(IN_ARBEIT);
            this.add(GESCHLOSSEN);
        }
    };
    
    public State(int id, String name) {
        this.id = id;
        this.name = name;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static List<State> getStatusList(){
        return statusList;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        
        if(obj != null){
            if(obj instanceof State){
                State status = (State) obj;
                
                if(status.getId() == this.getId()){
                    return true;
                }
            }
        }
        return false;
    }
    
    
}

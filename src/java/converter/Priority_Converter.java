/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import pojo.Priority;

/**
 *
 * @author efI
 */
public class Priority_Converter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {

         for(Priority p : Priority.getPriorities()) {
             if(p.getName().equals(string))
                return p;
             
         }
    
         return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {

        if(o != null){
           Priority p = (Priority) o;
           
           return p.getName();
        }
        
        return null;
    }
}

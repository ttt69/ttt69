/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import pojo.databasepojos.OSCL;
import user.User;
import user.Users;

/**
 *
 * @author Alex
 */
public class LoginBean implements Serializable {

    private String username;
    private String password;

    private boolean loggedIn;
    private User loggedUser = null;
    public List<OSCL> l = new ArrayList<>();

    FacesContext context = FacesContext.getCurrentInstance();
    Users users = (Users) context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);

    public LoginBean() {
        User u = new User(7, "Efraim", "", "Efraim", "Guias", "Mitarbeiter");
        users.addUser(u);
        u = new User(8, "Florian", "", "Florian", "Peischl", "Admin");
        users.addUser(u);
        u = new User(9, "Patrick", "", "Patrick", "Kopper", "Kunde");
        users.addUser(u);

    }

    public String doLogin() {
        // Get every user from our sample database :)
        String ret =null;
        for (User user : users.getUsers()) {
            if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                loggedUser = user;
                loggedIn = true;
                String role = user.getRolle();
                switch(role){
                    case "Kunde": ret= "filter_myView_kunde.xhtml?faces-redirect=true"; break;
                    case "Mitarbeiter": ret= "filter_myView_ma.xhtml?faces-redirect=true"; break;
                    case "Admin": ret= "filter_myView.xhtml?faces-redirect=true"; break; 
                }
            }
        }
  return ret;

    }
    
    public String getRole(){
        if(loggedUser != null){
            return loggedUser.getRolle();
        }
        return "";
    }
    
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;

        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "startPage.xhtml";
    }

    // ------------------------------
    // Getters & Setters 
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OSCP implements Serializable{
    
    private String prblmTypID;
    private String Name;
    private String Descriptio;

    public OSCP() {
    }

    public OSCP(String prblmTypID, String Name, String Descriptio) {
        this.prblmTypID = prblmTypID;
        this.Name = Name;
        this.Descriptio = Descriptio;
    }

    public String getPrblmTypID() {
        return prblmTypID;
    }

    public void setPrblmTypID(String prblmTypID) {
        this.prblmTypID = prblmTypID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescriptio() {
        return Descriptio;
    }

    public void setDescriptio(String Descriptio) {
        this.Descriptio = Descriptio;
    }

    @Override
    public boolean equals(Object obj) {
    boolean ret=false;
    
    if(obj!=null && obj instanceof OSCP){
        OSCP o=(OSCP) obj;
        
        if(this.prblmTypID.equals(o.prblmTypID)&&this.Descriptio.equals(o.Descriptio)){
            ret=true;
        }
    }
    return ret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.prblmTypID);
        hash = 41 * hash + Objects.hashCode(this.Name);
        hash = 41 * hash + Objects.hashCode(this.Descriptio);
        return hash;
    }
    
    
    
}

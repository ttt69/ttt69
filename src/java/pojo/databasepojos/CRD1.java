/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class CRD1 implements Serializable{
    
    private String Address;
    private String CardCode;
    private String Street;
    private String Block;
    private String ZipCode;
    private String City;
    private String Country;
    private String State;

    public CRD1() {
    }

    public CRD1(String Address, String CardCode, String Street, String Block, String ZipCode, String City, String Country, String State) {
        this.Address = Address;
        this.CardCode = CardCode;
        this.Street = Street;
        this.Block = Block;
        this.ZipCode = ZipCode;
        this.City = City;
        this.Country = Country;
        this.State = State;
    }

    @Override
    public boolean equals(Object obj) {
       boolean ret=false;
       
       if(obj!=null && obj instanceof CRD1){
           CRD1 c=(CRD1) obj;
           
           if(this.Address.equals(c.Address) && this.CardCode.equals(c.CardCode)&& this.ZipCode.equals(c.ZipCode)){
               ret=true;
           }
       }
       return ret;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.Address);
        hash = 41 * hash + Objects.hashCode(this.CardCode);
        hash = 41 * hash + Objects.hashCode(this.Street);
        hash = 41 * hash + Objects.hashCode(this.Block);
        hash = 41 * hash + Objects.hashCode(this.ZipCode);
        hash = 41 * hash + Objects.hashCode(this.City);
        hash = 41 * hash + Objects.hashCode(this.Country);
        hash = 41 * hash + Objects.hashCode(this.State);
        return hash;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getCardCode() {
        return CardCode;
    }

    public void setCardCode(String CardCode) {
        this.CardCode = CardCode;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getBlock() {
        return Block;
    }

    public void setBlock(String Block) {
        this.Block = Block;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String ZipCode) {
        this.ZipCode = ZipCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    @Override
    public String toString() {
        return "CRD1{" + "Address=" + Address + ", CardCode=" + CardCode + ", Street=" + Street + ", Block=" + Block + ", ZipCode=" + ZipCode + ", City=" + City + ", Country=" + Country + ", State=" + State + '}';
    }

    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import admin.AdminBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

/**
 *
 * @author efI
 */
public class Users {
    private static final int USERCOUNT=0;

    private List<User> users = new ArrayList<>();

    public Users() {
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User u) {
        users.add(u);
        System.out.println("User added");
    }

    public void addUse() {
        FacesContext context = FacesContext.getCurrentInstance();
        
        AdminBean adminBean = (AdminBean) context.getApplication().evaluateExpressionGet(context, "#{adminBean}", AdminBean.class);
        int i=0;
        for(User u: users){
            i = u.getId();
        }
        users.add(new User(++i, adminBean.getNewUsername(), adminBean.getNewPasswort(), adminBean.getNewRolle()));
        System.out.println("asdf");
    }
}

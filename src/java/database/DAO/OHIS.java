/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database.DAO;

/**
 *
 * @author Florian
 */
public class OHIS {
    //Pojo for History Ticket
    
    private String Code;
    private String Name;
    private String U_date;
    private int U_user;
    private String U_description;
    private double U_time;
    private int U_ticketnummer;

    public OHIS() {
    }

    public OHIS(String Code, String Name, String U_date, int U_user, String U_description, double U_time, int U_ticketnummer) {
        this.Code = Code;
        this.Name = Name;
        this.U_date = U_date;
        this.U_user = U_user;
        this.U_description = U_description;
        this.U_time = U_time;
        this.U_ticketnummer = U_ticketnummer;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getU_date() {
        return U_date;
    }

    public void setU_date(String U_date) {
        this.U_date = U_date;
    }

    public int getU_user() {
        return U_user;
    }

    public void setU_user(int U_user) {
        this.U_user = U_user;
    }

    public String getU_description() {
        return U_description;
    }

    public void setU_description(String U_description) {
        this.U_description = U_description;
    }

    public double getU_time() {
        return U_time;
    }

    public void setU_time(double U_time) {
        this.U_time = U_time;
    }

    public int getU_ticketnummer() {
        return U_ticketnummer;
    }

    public void setU_ticketnummer(int U_ticketnummer) {
        this.U_ticketnummer = U_ticketnummer;
    }

    @Override
    public String toString() {
        return "OHIS{" + "Code=" + Code + ", Name=" + Name + ", U_date=" + U_date + ", U_user=" + U_user + ", U_description=" + U_description + ", U_time=" + U_time + ", U_ticketnummer=" + U_ticketnummer + '}';
    }
    
    
    
    
}

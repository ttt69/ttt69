/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database.DAO;

import com.sap.smb.sbo.api.IBusinessPartners;
import com.sap.smb.sbo.api.ICompany;
import com.sap.smb.sbo.api.ICompanyService;
import com.sap.smb.sbo.api.IRecordset;
import com.sap.smb.sbo.api.IServiceCalls;
import com.sap.smb.sbo.api.SBOCOMConstants;
import com.sap.smb.sbo.api.SBOCOMUtil;
import com.sap.smb.sbo.api.SBOErrorMessage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javamail.MailUtils;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionManager {

  
    private static ConnectionManager connMgrInst = null;
    private ICompany company;
    private SBOErrorMessage errMsg = null;
    

    public static ConnectionManager getInst() {
        if (connMgrInst == null) {
            connMgrInst = new ConnectionManager();
        }
        return connMgrInst;
    }

    private ConnectionManager() {
        
     
    }

    public ICompany getConn() {
       
        int rc = 0;
        
        Properties prop_DB = new Properties();
        //Aus der Property Datei lesen
        InputStream in = MailUtils.class.getResourceAsStream("/properties/DatabaseConnection_Settings.properties");
        
        try {
            prop_DB.load(in);
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(MailUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {

            //10.100.101.120:30000
            company = SBOCOMUtil.newCompany();
            company.setServer(prop_DB.getProperty("Server"));
            company.setCompanyDB(prop_DB.getProperty("CompanyDB"));
            company.setUserName(prop_DB.getProperty("UserName"));
            company.setPassword(prop_DB.getProperty("Password"));
            
            company.setDbServerType(Integer.parseInt(prop_DB.getProperty("Password")));
            //company.setDbServerType(SBOCOMConstants.BoDataServerTypes_dst_MSSQL2012);
            System.out.println(SBOCOMConstants.BoDataServerTypes_dst_MSSQL2012 + SBOCOMConstants.BoDataServerTypes_dst_MSSQL2008);
            
            company.setLanguage(Integer.parseInt(prop_DB.getProperty("Language")));
            System.out.println(SBOCOMConstants.BoSuppLangs_ln_German);
            
            company.setDbUserName(prop_DB.getProperty("UserName"));
            company.setDbPassword(prop_DB.getProperty("DbPassword"));
            company.setLicenseServer(prop_DB.getProperty("LicenseServer"));

            rc = company.connect();
            if (rc == 0) {
                System.out.println("Connected!");

               

            } else {
                errMsg = company.getLastError();
                System.out.println(
                        "I cannot connect to database server: "
                        + errMsg.getErrorMessage()
                        + " "
                        + errMsg.getErrorCode());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Fehler bei Verbindung mit DB");
            
        }
        return company;

    }
}

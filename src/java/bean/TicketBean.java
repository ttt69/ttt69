/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

//import daotopojo.ConvertToPojo;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javamail.MailUtils;
import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.mail.Session;
import login.LoginData;
import org.primefaces.context.RequestContext;
import pojo.*;
import user.User;
import user.Users;

/**
 *
 * @author efI
 */
public class TicketBean implements Serializable {

    private String soft_type = "Software";
    private List<Ticket> filterTickets = new ArrayList<>();
    private List<Ticket> tickets = new ArrayList<>();
    private List<Ticket> myTickets = new ArrayList<>();
    private List<Ticket> softTickets = new ArrayList<>();
    private Ticket actTicket;
    private TimeZone timeZone = TimeZone.getDefault();
    private List<Ticket> histTickets = new ArrayList<>();
    private List<Ticket> rmaTickets = new ArrayList<>();
    private List<Ticket> openTickets = new ArrayList<>();
    private List<User> list_person_in_authority = new ArrayList();
    private List<Customer> customers = new ArrayList();
    private List<Priority> prioL = new ArrayList<>();
    private List<State> stateL = new ArrayList<>();
    private List<Typ> typL = new ArrayList<>();
    private List<SelectItem> loesu = new ArrayList<>();
    private List<SelectItem> leit = new ArrayList<>();
    private List<SelectItem> komm = new ArrayList<>();
    private List<SelectItem> vert = new ArrayList<>();
    private List<SelectItem> verr = new ArrayList<>();
    private List<Person> partners = new ArrayList<>();
    private Person new_partner;
    private Filtertick filter;
    private Ticket nextactTicket;
    private int testint;
    private int lfdTNr = 1;
    private int c_partner = 1;
    private List<SelectItem> person_in_authority_select_item = new ArrayList<>();
    private float total_time;

    private Boolean editable = false;
    private Boolean readOnly = true;

    //Instance for Settings-Bean
    FacesContext context = FacesContext.getCurrentInstance();
    Settings settings = (Settings) context.getApplication().evaluateExpressionGet(context, "#{settings}", Settings.class);
    LoginBean loginBean = (LoginBean) context.getApplication().evaluateExpressionGet(context, "#{loginBean}", LoginBean.class);
    Users users = (Users) context.getApplication().evaluateExpressionGet(context, "#{users}", Users.class);

    //Properties Objekte
    Properties prop_Cust = new Properties();
    Properties prop_PinA = new Properties();
    Properties prop_Cust_f = new Properties();

    public TicketBean() {

        initMenu();
    }

    public String newTicket() {
        String ret="";
        actTicket = new Ticket(lfdTNr++);
        GregorianCalendar now = new GregorianCalendar();
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        actTicket.setStartdate(now.getTime());
        list_person_in_authority = new ArrayList<>();
        list_person_in_authority.addAll(users.getUsers());
        person_in_authority_select_item = new ArrayList<>();
        for (User u : list_person_in_authority) {
            int anz = 0;
            for (int i : u.getNummern()) {
                anz++;
            }

            person_in_authority_select_item.add(new SelectItem(u, u.getUsername() + " " + anz));
        }
        if(loginBean.getRole().equals("Mitarbeiter")){
            ret = "newTicket_ma.xhtml";
        }
        if(loginBean.getRole().equals("Admin")){
            ret = "newTicket.xhtml";
        }
        return ret;
    }

    public String nextPage() {
        nextactTicket = actTicket;
        return "newTicket2.xhtml";
    }

    public String seeTicket(Ticket d) {
        readOnly = true;
        editable = false;
        return "newTicket.xhtml";
    }

    public String bearTicket(Ticket d) {
        actTicket = d;

        readOnly = false;
        editable = true;

        return "bearTicket";
    }

    public String checkDate(Ticket t) {

        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        Date now = new Date();
        Date lastupdate = actTicket.getLastmodified();
        now = calendar.getTime();
        //sdf.format(now);

        int diff = (int) (now.getTime() - lastupdate.getTime());

        if (diff > 3) {
            return "yellow";
        }
        if (diff > 5) {
//            return "red";
        }

        return null;

//        Date date1 = new Date();
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, -7);
//        date1 = cal.getTime();
//        Date date = null;
//        try {
//            date = sdf.parse(t.letztbearb);
//        } catch (ParseException ex) {
//            Logger.getLogger(TicketBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        if(date.before(date1) == true){
//            retVal = "red";
//        }
//        else
//            retVal = "green";
//        
//                
//        return retVal;
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();

        for (int i = 0; i < 10; i++) {
            results.add(query + i);
        }

        return results;
    }

    public String saveTicket() {

        Date date = new Date();
        //Info: Zeit setzen mit GCalender nicht mit Date Objekt
        GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("Europe/Vienna"));
        now.setTime(date);
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        //TimeZone.setDefault(TimeZone.getTimeZone("CET"));
        SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");

        Date d = new Date();
        System.out.println("Date: " + d.getTime());
        System.out.println("SDF: " + sd.format(d));
        System.out.println("Greg: " + now.getTime());

//        for(Ticket t: tickets){
//            if(t.ticketnummer != actTicket.ticketnummer){
//                actTicket.startdat = df.format(now.getTime());
//                tickets.add(actTicket);
//            }
//        }
        
        
        //falls Ticket neu angelegt wird...
        if (!tickets.contains(actTicket)) {

            actTicket.setStartdate(d);
            /*
            Session session = MailUtils.getGMailSession("adi.gal12a@gmail.com", "test123.");

            //Aus der Property Datei lesen
            InputStream in = getClass().getResourceAsStream("/properties/Mail_Customer.properties");
            try {
                prop_Cust.load(in);
                in = getClass().getResourceAsStream("/properties/Mail_PersonInAuthority.properties");
                prop_PinA.load(in);
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(TicketBean.class.getName()).log(Level.SEVERE, null, ex);
            }

            //E-Mail an den Zuständigen
            MailUtils.postMail(session, "adi.gal12a@gmail.com", prop_PinA.getProperty("Betreff") + " TNr" + actTicket.getTicketnumber(),
                    prop_PinA.getProperty("Anrede") + prop_PinA.getProperty("Nachricht") + prop_PinA.getProperty("Grussformel"));
            System.out.println("Email erfolgreich an den Zuständigen gesendet");

            //E-Mail an den Kunden
            MailUtils.postMail(session, "adi.gal12a@gmail.com", prop_Cust.getProperty("Betreff") + " TNr" + actTicket.getTicketnumber(),
                    prop_Cust.getProperty("Anrede") + prop_Cust.getProperty("Nachricht") + prop_Cust.getProperty("Grussformel"));
            System.out.println("Email erfolgreich an den Kunden gesendet");
            */
            tickets.add(actTicket);
            for (User u : users.getUsers()) {
                if (actTicket.getPerson_in_authority().getId() == u.getId()) {
                    u.setNummer(actTicket.getTicketnumber());
                }
            }
            
          //falls Ticket vorhanden...
        } else {
            actTicket.setLastmodified(d);
            /*
            //Überprüfe State
            if (actTicket.getState() == State.GESCHLOSSEN && actTicket.isMsentbefore() == false) {

                Session session = MailUtils.getGMailSession("adi.gal12a@gmail.com", "test123.");

                //Aus der Property Datei lesen
                InputStream in = getClass().getResourceAsStream("/properties/Mail_Customer_finished.properties");
                try {
                    prop_Cust_f.load(in);
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(TicketBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                //E-Mail an den Kunden
                MailUtils.postMail(session, "adi.gal12a@gmail.com", prop_Cust_f.getProperty("Betreff") + " TNr" + actTicket.getTicketnumber(),
                        prop_Cust_f.getProperty("Anrede") + prop_Cust_f.getProperty("Nachricht") + prop_Cust_f.getProperty("Grussformel"));
                System.out.println("Email erfolgreich an den Kunden gesendet");

                actTicket.setMsentbefore(true);
            }
            */
        }
        

        return "overview.xhtml";
    }

    public String deleteTicket(Ticket d) {
        tickets.remove(d);
        return null;
    }

    public void speichHist(AjaxBehaviorEvent ev) {
        GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("Europe/Vienna"));
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        /*
         float total_time = 0;
         if(actTicket.getHistory_list() != null && !actTicket.getHistory_list().isEmpty())
         total_time = actTicket.getHistory_list().get(actTicket.getHistory_list().size()-1).getTotal_time();

         actTicket.getHist().setTotal_time(total_time + actTicket.getHist().getTime());
         */
        actTicket.getHist().setDate(df.format(now.getTime()));
        actTicket.getHist().setUser(loginBean.getLoggedUser());
//        actTicket.getHist().setDescription(actTicket.getHist().getDescription());

        List<History> temp_list = new ArrayList<>();
        temp_list.addAll(actTicket.getHistory_list());
        temp_list.add(actTicket.getHist());
        actTicket.setHistory_list(temp_list);
        System.out.println(actTicket.getHistory_list());

        actTicket.setHist(new History());

        total_time = 0;
        for (History h : actTicket.getHistory_list()) {
            System.out.println(h.getTime());
            total_time = total_time + h.getTime();
        }
    }

    private void initMenu() {
        //Tickets

//        GregorianCalendar now = new GregorianCalendar(); 
//        SimpleDateFormat df = new SimpleDateFormat( "dd.MM.yyyy HH:mm" );
//        long time = now.getTimeInMillis();
//        now.setTimeInMillis(time+2000);
//       
//        String d = df.format(now.getTime());
        new_partner = new Person();
        actTicket = new Ticket();

        String d = null;
        //ConvertToPojo.initFromDB();
        //tickets = ConvertToPojo.getTickets();

        //Zuständigen - könnten User sein
        list_person_in_authority.addAll(users.getUsers());
//        list_person_in_authority.add(new Person(1, "Efraim", "Guias"));
//        list_person_in_authority.add(new Person(2, "Patrick", "Kopper"));
//        list_person_in_authority.add(new Person(3, "Florian", "Peischl"));

        //Kunden
        customers.add(new Customer(new Person(4, "Herbert", "Hut"), "K2001", "Intersport", "Produkt", 0, "Adresse"));
        customers.add(new Customer(new Person(5, "Franz", "Fuchs"), "K2002", "TAC Informationstechnologie GmbH", "Produkt", 0, "Adresse"));
        customers.add(new Customer(new Person(6, "Roland", "Meer"), "K2003", "Gruber-reisen", "Produkt", 0, "Adresse"));

        //customers.add(new Customer("K2002", "TAC Informationstechnologie GmbH", "Produkt", 0, "Adresse", 5, "Vorname", "Nachname"));
        //customers.add(new Customer("K2003", "Gruber-reisen", "Produkt", 0, "Adresse", 6, "Vorname", "Nachname"));
        //Tickets
        tickets.add(new Ticket(lfdTNr++, soft_type, State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(0), new Date(114, 10, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type, State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.NEUTRAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(0), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.TRIVIAL, (Customer) customers.get(1), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.IN_ARBEIT, "Fehler bei der Installation von SAP", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Fertigstellung Prototyp", list_person_in_authority.get(0), Priority.NEUTRAL, (Customer) customers.get(0), new Date(114, 5, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Implementierung", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 6, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.OFFEN, "Druckfunktion", list_person_in_authority.get(2), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 7, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        tickets.add(new Ticket(lfdTNr++, soft_type,  State.GESCHLOSSEN, "Bug im Programm", list_person_in_authority.get(0), Priority.WICHTIG, (Customer) customers.get(2), new Date(114, 8, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, true));
        tickets.add(new Ticket(lfdTNr++, soft_type, State.IN_ARBEIT, "Programm stürtzt ab", list_person_in_authority.get(1), Priority.WICHTIG, (Customer) customers.get(1), new Date(114, 9, 30), null, null, null, null, null, null, null, null, null, null, true, true, null, null, null, false));
        
        for (Ticket t : tickets) {
            System.out.println("Ticket " + t.getTicketnumber());
            for (User u : list_person_in_authority) {
                if (t.getPerson_in_authority().equals(u)) {
                    u.setNummer(t.getTicketnumber());
                }
            }
        }
        for (User u : list_person_in_authority) {
            int anz = 0;
            for (int i : u.getNummern()) {
                anz++;
            }
            System.out.println("Anzahl " + anz);

            person_in_authority_select_item.add(new SelectItem(u, u.getUsername() + " " + anz));
        }

        //Priorität
        prioL.add(settings.getDefaultPrio());

        for (Priority priority : Priority.getPriorities()) {
            if (priority.getId() != settings.getDefaultPrio().getId()) {
                prioL.add(priority);
            }
        }

        //Typ
        typL.add(settings.getDefaultTyp());

        for (Typ t : Typ.getTypes()) {
            if (t.getId() != settings.getDefaultTyp().getId()) {
                typL.add(t);
            }
        }

        //Status
        stateL.add(settings.getDefaultStatus());

        for (State s : State.getStatusList()) {
            if (s.getId() != settings.getDefaultStatus().getId()) {
                stateL.add(s);
            }
        }

        //Vertrag
        vert.add(new SelectItem("Projektrealisierung"));
        vert.add(new SelectItem("Support"));
        vert.add(new SelectItem("Wartung"));

        //Leistung
        leit.add(new SelectItem("Support Allgemein"));
        leit.add(new SelectItem("Support CoreSuite"));
        leit.add(new SelectItem("Support MARI PROJECT"));
        leit.add(new SelectItem("Dienstleistung abC"));
        leit.add(new SelectItem("Dienstleistung fremd"));
        leit.add(new SelectItem("Programmierung abC"));

        //Lösungsmethode
        loesu.add(new SelectItem("Erledigt"));
        loesu.add(new SelectItem("Unerledigt"));
        loesu.add(new SelectItem("Doppelt"));
        loesu.add(new SelectItem("Unvollständig"));
        loesu.add(new SelectItem("Nicht reproduzierbar"));

        komm.add(new SelectItem("Telefon"));
        komm.add(new SelectItem("E-Mail"));

        verr.add(new SelectItem("Verrechenbar"));

        //Ansprechpartner
        partners.add(new Person(c_partner++, "Thomas", "Froh"));
        partners.add(new Person(c_partner++, "Siegfried", "Hofer"));

        //Meine Tickets
        
        
    }

    public void checkTime() {
        GregorianCalendar now = new GregorianCalendar();
        long actTime = now.getTimeInMillis();
        for (Ticket t : tickets) {
            String ti = t.getDeadline();
            long time = t.getEndedate().getTime();
            if (actTime == time) {
                FacesMessage fm = new FacesMessage("Folgendes Ticket muss bearbeitet werden", t.getSubject());
            }

        }
    }

    public void increment() {
        testint++;
    }

    public void checkPoll(AjaxBehaviorEvent ev) {
        testint++;
        System.out.println("checkPoll");
        if (testint == 10) {
            FacesMessage fm = new FacesMessage("funktioniert");
        }
    }

    public void addContact_Person(AjaxBehaviorEvent ev) {
        System.out.println("addContact geht");

        System.out.println(new_partner);

        partners.add(new Person(c_partner, new_partner.getFirstname(), new_partner.getSurname()));
        c_partner++;
        new_partner = new Person();
    }

    public void initPrioList(AjaxBehaviorEvent ev) {

        prioL.clear();
        prioL.add(settings.getDefaultPrio());

        for (Priority priority : Priority.getPriorities()) {
            if (settings.getDefaultPrio().getId() != priority.getId()) {
                prioL.add(priority);
            }
        }
    }

    public void initTypList(AjaxBehaviorEvent ev) {

        typL.clear();
        typL.add(settings.getDefaultTyp());

        for (Typ t : Typ.getTypes()) {
            if (settings.getDefaultTyp().getId() != t.getId()) {
                typL.add(t);
            }
        }
    }

    public void initStatusList(AjaxBehaviorEvent ev) {

        stateL.clear();
        stateL.add(settings.getDefaultStatus());

        for (State s : State.getStatusList()) {
            if (s.getId() != settings.getDefaultStatus().getId()) {
                stateL.add(s);
            }
        }
    }

    public void closeDialog() {
        RequestContext rc = RequestContext.getCurrentInstance();

        rc.execute("PF('dlg').hide();");
    }

    public List<Ticket> histTicket() {
        histTickets = new ArrayList<>();
        for (Ticket t : tickets) {
            if (t.getState() == State.GESCHLOSSEN) {
                histTickets.add(t);
            }
        }
        return histTickets;
    }
    
    public List<Ticket> openTicket() {
        openTickets = new ArrayList<>();
        for (Ticket t : tickets) {
            if (t.getState() == State.OFFEN) {
                openTickets.add(t);
            }
        }
        return openTickets;
    }
    
    
    
    public List<Ticket> hardTicket(){
        rmaTickets = new ArrayList<>();
        for(Ticket t : tickets){
            if(t.getTicketType().equals("Hardware")){
                rmaTickets.add(t);
            }
        }
        return rmaTickets;
    }
    
    public List<Ticket> softTicket() {
        softTickets = new ArrayList<>();
        for(Ticket t: tickets) {
            if(t.getTicketType().equals("Software")){
                softTickets.add(t);
           }
        }
        return softTickets;
        
    }
    
    public List<Ticket> myTickets(){
        myTickets = new ArrayList<>();
        for (Ticket t : tickets) {
            if (t.getPerson_in_authority().equals(loginBean.getLoggedUser())) {
                myTickets.add(t);
            }
           
        } return myTickets;
    }

    //Filter
    public boolean filterByAny(Object value, Object filter, Locale locale) {

        String filterText = (filter == null) ? null : filter.toString().trim();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        if (value == null) {
            return false;
        }

        String anyvalue = value.toString().toLowerCase();
        filterText = filterText.toLowerCase();

        if (anyvalue.contains(filterText)) {
            return true;
        } else {
            return false;
        }

    }

    public String checkColor(Ticket t) {

        String ret = "green";
        Calendar c = new GregorianCalendar();
        c.add(Calendar.DATE, -5);
        
        /*
        if (t.getState().getId() == 1 || t.getState().getId() == 2) {

            if (t.getLastmodified() != null) {
                if ((t.getLastmodified().compareTo(c.getTime())) < 0) {
                    ret = "red";
                }
            } else {
                ret = "red";
            }
        }*/
        
        
        String prio = t.getPriority().toString();
        System.out.println(prio);
        switch (prio) {
            case "Wichtig":  ret = "red";
                     break;
            case "Neutral":  ret = "green";
                     break;
            case "Trivial":  ret = "orange";
                     break;
        }
        return ret;
    }

    public void customerselected(Customer c) {
        actTicket.setCustomer(c);
    }

    //GETTER AND SETTER
    public List<Priority> getPrioL() {
        return prioL;
    }

    public void setPrioL(List<Priority> prioL) {
        this.prioL = prioL;
    }

    public List<State> getStateL() {
        return stateL;
    }

    public void setStateL(List<State> stateL) {
        this.stateL = stateL;
    }

    public List<SelectItem> getLoesu() {
        return loesu;
    }

    public void setLoesu(List<SelectItem> loesu) {
        this.loesu = loesu;
    }

    public List<SelectItem> getLeit() {
        return leit;
    }

    public void setLeit(List<SelectItem> leit) {
        this.leit = leit;
    }

    public List<Typ> getTypL() {
        return typL;
    }

    public void setTypL(List<Typ> typL) {
        this.typL = typL;
    }

    public List<SelectItem> getKomm() {
        return komm;
    }

    public void setKomm(List<SelectItem> komm) {
        this.komm = komm;
    }

    public List<SelectItem> getVert() {
        return vert;
    }

    public void setVert(List<SelectItem> vert) {
        this.vert = vert;
    }

    public List<SelectItem> getVerr() {
        return verr;
    }

    public void setVerr(List<SelectItem> verr) {
        this.verr = verr;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public void filtTick() {
        filter = new Filtertick((ArrayList<Ticket>) tickets);
    }

    public Filtertick getFilter() {
        return filter;
    }

    public void setFilter(Filtertick filter) {
        this.filter = filter;
    }

    public void setFilterTickets(List<Ticket> filterTickets) {
        this.filterTickets = filterTickets;
    }

    public List<Ticket> getFilterTickets() {
        return filterTickets;
    }

    public Ticket getNextactTicket() {
        return nextactTicket;
    }

    public void setNextactTicket(Ticket nextactTicket) {
        this.nextactTicket = nextactTicket;
    }

    public List<Person> getPartners() {
        return partners;
    }

    public void setPartners(List<Person> partners) {
        this.partners = partners;
    }

    public Person getNew_partner() {
        return new_partner;
    }

    public void setNew_partner(Person new_partner) {
        this.new_partner = new_partner;
    }

    public int getTestint() {
        return testint;
    }

    public void setTestint(int testint) {
        this.testint = testint;
    }

    public FacesContext getContext() {
        return context;
    }

    public void setContext(FacesContext context) {
        this.context = context;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public List<Ticket> getHistTickets() {
        return histTickets;
    }

    public void setHistTickets(List<Ticket> histTickets) {
        this.histTickets = histTickets;
    }

    public List<Ticket> getSoftTickets() {
        return softTickets;
    }

    public void setSoftTickets(List<Ticket> softTickets) {
        this.softTickets = softTickets;
    }
    
    

    public Boolean isEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getEditable() {
        return editable;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public List<User> getList_person_in_authority() {
        return list_person_in_authority;
    }

    public void setList_person_in_authority(List<User> list_person_in_authority) {
        this.list_person_in_authority = list_person_in_authority;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public int getLfdTNr() {
        return lfdTNr;
    }

    public void setLfdTNr(int lfdTNr) {
        this.lfdTNr = lfdTNr;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public List<Ticket> getMyTickets() {
        return myTickets;
    }

    public void setMyTickets(List<Ticket> myTickets) {

        this.myTickets = myTickets;
    }

    public Ticket getActTicket() {
        return actTicket;
    }

    public void setActTicket(Ticket actTicket) {
        this.actTicket = actTicket;
    }

    public List<SelectItem> getPerson_in_authority_select_item() {
        return person_in_authority_select_item;
    }

    public void setPerson_in_authority_select_item(List<SelectItem> person_in_authority_select_item) {
        this.person_in_authority_select_item = person_in_authority_select_item;
    }

    public int getC_partner() {
        return c_partner;
    }

    public void setC_partner(int c_partner) {
        this.c_partner = c_partner;
    }

    public float getTotal_time() {
        return total_time;
    }

    public void setTotal_time(float total_time) {
        this.total_time = total_time;
    }

    public List<Ticket> getRmaTickets() {
        return rmaTickets;
    }

    public void setRmaTickets(List<Ticket> rmaTickets) {
        this.rmaTickets = rmaTickets;
    }
    

}

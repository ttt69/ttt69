/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojo.databasepojos;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Florian
 */
public class OCRD implements Serializable{
    private String CardCode;
    private String CardName;
    private String CardType;
    private String GroupCode;
    private String CmpPrivate;
    private String Address;
    private String ZipCode;
    private String MailAddres;
    private String MailZipCod;
    private String Phone1;
    private String Phone2;
    private String Fax;
    private String CntctPrsn;

    public OCRD() {
    }

    public OCRD(String CardCode, String CardName, String CardType, String GroupCode, String CmpPrivate, String Address, String ZipCode, String MailAddres, String MailZipCod, String Phone1, String Phone2, String Fax, String CntctPrsn) {
        this.CardCode = CardCode;
        this.CardName = CardName;
        this.CardType = CardType;
        this.GroupCode = GroupCode;
        this.CmpPrivate = CmpPrivate;
        this.Address = Address;
        this.ZipCode = ZipCode;
        this.MailAddres = MailAddres;
        this.MailZipCod = MailZipCod;
        this.Phone1 = Phone1;
        this.Phone2 = Phone2;
        this.Fax = Fax;
        this.CntctPrsn = CntctPrsn;
    }

    
    
    public String getCardCode() {
        return CardCode;
    }

    public void setCardCode(String CardCode) {
        this.CardCode = CardCode;
    }

    public String getCardName() {
        return CardName;
    }

    public void setCardName(String CardName) {
        this.CardName = CardName;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String CardType) {
        this.CardType = CardType;
    }

    public String getGroupCode() {
        return GroupCode;
    }

    public void setGroupCode(String GroupCode) {
        this.GroupCode = GroupCode;
    }

    public String getCmpPrivate() {
        return CmpPrivate;
    }

    public void setCmpPrivate(String CmpPrivate) {
        this.CmpPrivate = CmpPrivate;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String ZipCode) {
        this.ZipCode = ZipCode;
    }

    public String getMailAddres() {
        return MailAddres;
    }

    public void setMailAddres(String MailAddres) {
        this.MailAddres = MailAddres;
    }

    public String getMailZipCod() {
        return MailZipCod;
    }

    public void setMailZipCod(String MailZipCod) {
        this.MailZipCod = MailZipCod;
    }

    public String getPhone1() {
        return Phone1;
    }

    public void setPhone1(String Phone1) {
        this.Phone1 = Phone1;
    }

    public String getPhone2() {
        return Phone2;
    }

    public void setPhone2(String Phone2) {
        this.Phone2 = Phone2;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String Fax) {
        this.Fax = Fax;
    }

    public String getCntctPrsn() {
        return CntctPrsn;
    }

    public void setCntctPrsn(String CntctPrsn) {
        this.CntctPrsn = CntctPrsn;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret=false;
        
        if(obj!=null){
            if(obj instanceof OCRD){
                
                OCRD d=(OCRD) obj;
                
                if(d.CardCode.equals(this.CardCode)){
                    ret=true;
                }
            }
        }
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.CardCode);
        hash = 83 * hash + Objects.hashCode(this.CardName);
        hash = 83 * hash + Objects.hashCode(this.CardType);
        hash = 83 * hash + Objects.hashCode(this.Address);
        hash = 83 * hash + Objects.hashCode(this.Phone1);
        hash = 83 * hash + Objects.hashCode(this.CntctPrsn);
        return hash;
    }
    
    
    
    
}
